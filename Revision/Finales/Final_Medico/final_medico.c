#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
#define ARCHIVOPACIENTES "PACIENTES.DAT"
#define ARCHIVOCONSULTAS "CONSULTAS.DAT"


typedef struct sPaciente
{

  int NroHistoriaClinica;
  char Nombre[50];
  char Prepaga[40];

}tpaciente;

typedef struct sConsulta{
  int NroHistoriaClinica;
  char Diagnostico[40];
  char Fecha[9]; //AAAAMMDD

}tconsulta;

int cargaPaciente(char nombrePacientes[]);
int cargaConsulta(char nombrePacientes[],char nombreConsulta[]);
int busquedaPaciente (char nombrePacientes[], char nombreConsulta[]);

int main()

{
  //cargaPaciente(ARCHIVOPACIENTES);
  //cargaConsulta(ARCHIVOPACIENTES,ARCHIVOCONSULTAS);
  busquedaPaciente(ARCHIVOPACIENTES,ARCHIVOCONSULTAS);
  return 0;


}

int cargaPaciente(char nombrePacientes[])
{
    FILE *fb;
    tpaciente reg,reg2;
    int cantReg = 0;
    int i = 0;
    int j = 0;
    int terminado = 0;
    char cad[80];
    char c;

    fb = fopen(nombrePacientes,"rb+");
    if(fb == NULL)
      return 1;

    fseek(fb,0,SEEK_END);
    cantReg = ftell(fb)/sizeof(reg);


    printf("Ingrese Nro HClinica: ");
    scanf("%d",&reg.NroHistoriaClinica);


    while(reg.NroHistoriaClinica != -1)
    {
      fread(&reg2,sizeof(reg2),1,fb);
      while(!feof(fb))
      {

            if(reg.NroHistoriaClinica == reg2.NroHistoriaClinica)
              {
                printf("Previo valor existe, Ingrese Nro HClinica: ");
                scanf("%d",&reg.NroHistoriaClinica);
                fseek(fb,0,SEEK_SET);

              }

            fread(&reg2,sizeof(reg2),1,fb);

      }




    while(getchar()!='\n')
      ;

    printf("Ingrese Nombre: ");
    gets(cad);
    strcpy(reg.Nombre,cad);
    printf("Ingrese Prepaga: ");
    gets(cad);
    strcpy(reg.Prepaga,cad);
    fwrite(&reg,sizeof(reg),1,fb);

    printf("\n\n");
    printf("Ingrese Nro HClinica: ");
    scanf("%d",&reg.NroHistoriaClinica);
    fseek(fb,0,SEEK_SET);
  }


    fclose(fb);

    return 0;

}


int cargaConsulta(char nombrePacientes[], char nombreConsulta[])
{

  FILE *fbp;
  FILE *fbc;
  tpaciente reg_p;
  tconsulta reg_c;
  int aux = 0;
  char cad[80];
  int i = 0;
  int flag = 0;

  fbp=fopen(nombrePacientes,"rb+");
  fbc=fopen(nombreConsulta,"rb+");

  if(fbp == NULL || fbc == NULL)
    return 1;

  printf("Ingrese NroHistoriaClinica: ");
  scanf("%d",&aux);

  fread(&reg_p,sizeof(reg_p),1,fbp);

  while(!feof(fbp))
  {
        if(reg_p.NroHistoriaClinica == aux && flag == 0)
        {
              reg_c.NroHistoriaClinica = aux;
              while(getchar()!='\n')
                ;
              printf("Ingrese Diagnostico: ");
              gets(reg_c.Diagnostico);
              printf("Ingrese Fecha AAAAMMDD:");
              gets(reg_c.Fecha);
              fwrite(&reg_c,sizeof(reg_c),1,fbc);
              flag = 1;

        }

        fread(&reg_p,sizeof(reg_p),1,fbp);


  }

  if (flag==0)
    printf("\nNro Historia Clinica Inexistente\n");

  fclose(fbp);
  fclose(fbc);
  return 0;


}


int busquedaPaciente(char nombrePacientes[], char nombreConsulta[])
{

  FILE *fbp, *fbc;
  tpaciente reg_p;
  tconsulta reg_c;
  char cad[50];
  int i = 0;
  int j = 0;
  int cantReg = 0;
  int aux_nh=0;


  fbp = fopen(nombrePacientes,"rb+");
  fbc= fopen (nombreConsulta,"rb+");

  if(fbp==NULL||fbc==NULL)
    return 1;

  printf("Ingrese Nombre Paciente:");
  gets(cad);

  fread(&reg_p,sizeof(reg_p),1,fbp);

  while(!feof(fbp))
  {

    if(strstr(reg_p.Nombre,cad)!=NULL)
      {
          aux_nh = reg_p.NroHistoriaClinica;

      }

    fread(&reg_p,sizeof(reg_p),1,fbp);

  }

  fread(&reg_c,sizeof(reg_c),1,fbc);

  while(!feof(fbc))
  {

    if(reg_c.NroHistoriaClinica == aux_nh)
    {
        printf("\n%d",reg_c.NroHistoriaClinica );
        printf("\n%s",reg_c.Diagnostico );
        printf("\n%s",reg_c.Fecha );

    }


    fread(&reg_c,sizeof(reg_c),1,fbc);
  }

  fclose(fbp);
  fclose(fbc);

  return 0;

}
