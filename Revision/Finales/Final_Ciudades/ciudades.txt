San Nicolas                     -33.330021  -60.240002      117124  Buenos Aires
Corrientes                      -27.489964  -58.809986      339945  Corrientes
Parana                          -31.733322  -60.533344      226852  Entre Rios
La Plata                        -34.909615  -57.959961      440389  Buenos Aires
Concordia                       -31.389570  -58.029984      132761  Entre Rios
San Luis                        -33.299995  -66.350021      308146  San Luis
Rio Cuarto                      -33.130032  -64.349983      135960  Cordoba
San Salvador de Jujuy           -24.183344  -65.300034      258739  Jujuy
Catamarca                       -28.470009  -65.779999      162586  Catamarca
La Rioja                        -29.409950  -66.849960      147130  La Rioja
Santiago del Estero             -27.783331  -64.266655      317550  Santiago del Estero
Resistencia                     -27.459991  -58.990028      368456  Chaco
San Juan                        -31.550026  -68.519989      433892  San Juan
Neuquen                         -38.950039  -68.059990      213824  Neuquen
Salta                           -24.783360  -65.416641      484646  Salta
San Miguel de Tucuman           -26.816000  -65.216621      678804  Tucuman
Formosa                         -26.172834  -58.182816      202272  Formosa
Santa Fe                        -31.623873  -60.690002      393504  Santa Fe
Rosario                         -32.951130  -60.666309     1094785  Santa Fe
Comodoro Rivadavia              -45.870029  -67.500000      123291  Chubut
Mendoza                         -32.883331  -68.816612      827815  Mendoza
Bahia Blanca                    -38.740028  -62.265022      279041  Buenos Aires
Mar del Plata                   -38.000019  -57.579983      554916  Buenos Aires
Cordoba                         -31.399958  -64.182297     1374468  Cordoba
Posadas                         -27.357832  -55.885105      334590  Misiones
Buenos Aires                    -34.602501  -58.397530    11862073  Buenos Aires
