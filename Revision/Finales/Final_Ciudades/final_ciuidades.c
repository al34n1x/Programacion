/*
Una empresa de distribuci�n de mercader�as dispone de un archivo binario con algunos datos de las principales
ciudades del pa�s, y sus registros responden a la siguiente estructura:

struct sciudad {
char nombre[40];
float latitud;
float longitud;
int poblacion;
char provincia[30];
};


El examen consiste en:

1. Ordenar el archivo binario por provincia, en orden alfab�tico ascendente, utilizando cualquier m�todo
de ordenamiento estudiado. Recuerde que los archivos no pueden cargarse totalmente en memoria. (hasta 25%
del examen)
2. Imprimir un listado con el total de habitantes de cada provincia y el total del pa�s. (hasta 25% del examen)
3. Realizar un borrado l�gico de aquellas ciudades cuya poblaci�n sea menor a 10000 habitantes, mostrando por
pantalla los registros eliminados. (hasta 25% del examen)
4. Generar un archivo de texto a partir del archivo binario, donde cada l�nea contenga los campos incluidos en cada
registro del archivo original, con excepci�n de los registros eliminados. El archivo de texto deber� generarse
perfectamente encolumnado, con los campos de cadenas alineados sobre el margen izquierdo, los campos num�ricos
alineados sobre el derecho y dos espacios de separaci�n entre columnas, tal como se muestra a continuaci�n.
(hasta 25% del examen)

Ejemplo del archivo de texto a generar  ([......] significa que se omiten algunas l�neas)

Azul                          -36.779630  -59.870000      43408  Buenos Aires
Bahia Blanca                  -38.740027  -62.265021     279041  Buenos Aires
Buenos Aires                  -34.602502  -58.397531   11862073  Buenos Aires
[......]
Viedma                        -40.799953  -63.000015      54031  Buenos Aires
Belen                         -27.649593  -67.033283      11359  Catamarca
[......]
Tinogasta                     -28.066621  -67.566584        587  Catamarca
Charata                       -27.216286  -61.199996      18297  Chaco
Juan Jose Castelli            -25.949541  -60.616647       9421  Chaco
[......]

Se suministra un archivo binario a modo de ejemplo llamado �CIUDADES.DAT�. Tener en cuenta que este archivo
es s�lo una muestra. El programa debe funcionar para cualquier archivo con la misma estructura, aunque tenga
distinta cantidad de registros o provincias.
*/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <time.h>
#define ARCHIVOBINARIO "ciudades.dat"
#define ARCHIVOTEXTO "ciuidades.txt"

typedef struct sciudad {

  char nombre[40];
  float latitud;
  float longitud;
  int poblacion;
  char provincia[30];

} tciudad;


int ordenarArchiboBinario(char nombreBinario[]);
int imprimirArchivoBinario(char nombreBinario[]);
int listadoCantidades(char nombreBinario[]);
int borradoLogico(char nombreBinario[]);
int generarArchivoTexto(char nombreBinario[], char nombreTexto[]);
int generarArchivoBinario(char nombreTexto[],char nombreBinario[]);


int main()
{


  //ordenarArchiboBinario(ARCHIVOBINARIO);
  //imprimirArchivoBinario(ARCHIVOBINARIO);
  //listadoCantidades(ARCHIVOBINARIO);
  //borradoLogico(ARCHIVOBINARIO);
  //generarArchivoTexto(ARCHIVOBINARIO,ARCHIVOTEXTO);
  generarArchivoBinario(ARCHIVOTEXTO,ARCHIVOBINARIO);
  imprimirArchivoBinario(ARCHIVOBINARIO);
  return 0;


}


int ordenarArchiboBinario(char nombreBinario[])
{

    FILE *arch;
    tciudad prov1;
    tciudad prov2;
    int i = 0;
    int j = 0;
    int cantReg = 0;
    int terminar = 0;

    arch = fopen (nombreBinario, "rb+");
    if(arch == NULL)
    {
        printf("No se pudo abrir el archivo\n");
        system ("pause");
        return 1;
    }

    fseek(arch,0,SEEK_END);
    cantReg = ftell(arch)/sizeof(tciudad);
    i=i+1;
    while (terminar == 0)
    {
      terminar = 1;
      for (j=0; j<cantReg-i; j++)
      {

            fseek(arch,j*sizeof(prov1), SEEK_SET);
            fread(&prov1,sizeof(prov1),1,arch);
            fread(&prov2,sizeof(prov2),1,arch);
            if (strcmp(prov1.provincia,prov2.provincia)>0)
            {
                fseek(arch,-2*sizeof(prov1), SEEK_CUR);
                fwrite(&prov2,sizeof(prov2),1,arch);
                fwrite(&prov1,sizeof(prov1),1,arch);
                terminar = 0;
            }

        }

        i++;
    }


    fclose(arch);

    return 0;
}

int imprimirArchivoBinario(char nombreBinario[])
{

  FILE *arch;
  tciudad ciudad;

  arch = fopen(nombreBinario, "rb+");
  if (arch == NULL)
  {
    printf("No se pudo abrir el archivo\n");
    return 1;

  }

  fread(&ciudad,sizeof(ciudad),1,arch);

  while(!feof(arch))
  {

    puts(ciudad.nombre);
    printf("%f\t", ciudad.latitud);
    printf("%4f\t", ciudad.longitud);
    printf("%4d\t", ciudad.poblacion);
    puts(ciudad.provincia);
    fread(&ciudad,sizeof(ciudad),1,arch);
    printf("\n");

  }

  fclose(arch);
  return 0;



}

int listadoCantidades(char nombreBinario[])
{
    FILE *arch;
    int cant = 0;
    int i = 0;
    tciudad reg;
    int totalProvincia = 0;
    int totalPais = 0;
    char provinciaAnterior[80];



    arch = fopen(nombreBinario,"rb+");
    if (arch == NULL)
    {
        printf("No se pudo abrir archivo");
        return 1;

    }


    fread(&reg, sizeof(reg),1,arch);
    strcpy(provinciaAnterior, reg.provincia);


    while(!feof(arch))
    {
        if (strcmp(provinciaAnterior,reg.provincia)==0)
          totalProvincia = totalProvincia + reg.poblacion;
        else
          {
            printf("\nTotal poblacion de %s es %d", provinciaAnterior,totalProvincia);
            totalProvincia = reg.poblacion;
            strcpy(provinciaAnterior, reg.provincia);
          }

        totalPais = totalPais + reg.poblacion;
        fread(&reg, sizeof(reg),1,arch);


    }

    printf("\nTotal poblacion de %s es %d", provinciaAnterior,totalProvincia);
    printf("\nTotal Pais es %d", totalPais);


    fclose(arch);

    return 0;


}

int borradoLogico(char nombreBinario[])
{

  FILE *arch;
  tciudad reg;
  char ciudad[80];

  arch = fopen(nombreBinario,"rb+");
  if (arch == NULL)
  {
      printf("No se pudo abrir el archivo");
      return 1;

  }

  fread(&reg,sizeof(reg),1,arch);
  strcpy(ciudad,reg.nombre);

  while(!feof(arch))
  {
        if(reg.poblacion<10000)
          {
              reg.poblacion = -1;
              fseek(arch,-1*sizeof(reg),SEEK_CUR);
              fwrite(&reg,sizeof(reg),1,arch);
              printf("\nSe ha eliminado la ciudad %s", ciudad);

          }

        fread(&reg,sizeof(reg),1,arch);
        strcpy(ciudad,reg.nombre);


  }

  fclose(arch);
  return 0;

}

int generarArchivoTexto(char nombreBinario[],char nombreTexto[])
{

    FILE *arch_bin;
    FILE *arch_text;
    tciudad reg;
    tciudad reg2;

    arch_bin=fopen(nombreBinario,"rb+");
    if(arch_bin==NULL)
    {
        printf("No se pudo abrir el archivo");
        return 1;

    }


    arch_text=fopen(nombreTexto,"wb+");
    if(arch_text==NULL)
    {
        printf("No se pudo abrir el archivo");
        return 1;

    }

    fread(&reg,sizeof(reg),1,arch_bin);

    while(!feof(arch_bin))
    {
        if(reg.poblacion>0)
        {
          fprintf(arch_text, "%s  %.4f  %.4f  %d  %s\n",reg.nombre,reg.latitud,reg.longitud,reg.poblacion,reg.provincia );
        }
        fread(&reg,sizeof(reg),1,arch_bin);
    }

  fclose(arch_text);
  fclose(arch_bin);
  return 0;

}

int generarArchivoBinario(char nombreTexto[],char nombreBinario[])
{
  FILE *arch_text;
  FILE *arch_bin;
  tciudad reg;
  char cadAux[256];
  char c;
  int i=0;

  arch_text = fopen(nombreTexto,"rb+");
  arch_bin = fopen(nombreBinario,"wb+");

  if(arch_text==NULL || arch_bin == NULL)
  {
      printf("No se pudo abrir archivo");
      return 1;

  }

  c=fgetc(arch_text);

  while(!feof(arch_text)){
  while(c!=';' && c!='\n')
  {
      cadAux[i]=c;
      i++;
      c=fgetc(arch_text);
  }

  cadAux[i]='\0';
  strcpy(reg.nombre,cadAux);
  i=0;
  c=fgetc(arch_text);

  while(c!=';' && c!='\n')
  {
      cadAux[i]=c;
      i++;
      c=fgetc(arch_text);
  }

  cadAux[i]='\0';
  sscanf(cadAux, "%4f",&reg.latitud);
  c=fgetc(arch_text);

  while(c!=';' && c!='\n')
  {
      cadAux[i]=c;
      i++;
      c=fgetc(arch_text);
  }

  cadAux[i]='\0';
  sscanf(cadAux, "%4f",&reg.longitud);
  i=0;
  c=fgetc(arch_text);

  while(c!=';' && c!='\n')
  {
      cadAux[i]=c;
      i++;
      c=fgetc(arch_text);
  }

  cadAux[i]='\0';
  sscanf(cadAux, "%4d",&reg.poblacion);
  i=0;
  c=fgetc(arch_text);


  while(c!=';' && c!='\n')
  {
      cadAux[i]=c;
      i++;
      c=fgetc(arch_text);
  }

  cadAux[i]='\0';
  strcpy(reg.provincia,cadAux);
  i=0;
  fwrite(&reg,sizeof(reg),1,arch_bin);
  c=fgetc(arch_text);
  }
  fclose(arch_text);
  fclose(arch_bin);
  return 0;

}
