/*
Una empresa de distribuci�n de mercader�as dispone de un archivo binario con algunos datos de las principales
ciudades del pa�s, y sus registros responden a la siguiente estructura:

struct sciudad {
char nombre[40];
float latitud;
float longitud;
int poblacion;
char provincia[30];
};
	

El examen consiste en:

1. Ordenar el archivo binario por provincia, en orden alfab�tico ascendente, utilizando cualquier m�todo
de ordenamiento estudiado. Recuerde que los archivos no pueden cargarse totalmente en memoria. (hasta 25%
del examen)
2. Imprimir un listado con el total de habitantes de cada provincia y el total del pa�s. (hasta 25% del examen)
3. Realizar un borrado l�gico de aquellas ciudades cuya poblaci�n sea menor a 10000 habitantes, mostrando por
pantalla los registros eliminados. (hasta 25% del examen)
4. Generar un archivo de texto a partir del archivo binario, donde cada l�nea contenga los campos incluidos en cada
registro del archivo original, con excepci�n de los registros eliminados. El archivo de texto deber� generarse
perfectamente encolumnado, con los campos de cadenas alineados sobre el margen izquierdo, los campos num�ricos
alineados sobre el derecho y dos espacios de separaci�n entre columnas, tal como se muestra a continuaci�n.
(hasta 25% del examen)

Ejemplo del archivo de texto a generar  ([......] significa que se omiten algunas l�neas)

Azul                          -36.779630  -59.870000      43408  Buenos Aires
Bahia Blanca                  -38.740027  -62.265021     279041  Buenos Aires
Buenos Aires                  -34.602502  -58.397531   11862073  Buenos Aires
[......]
Viedma                        -40.799953  -63.000015      54031  Buenos Aires
Belen                         -27.649593  -67.033283      11359  Catamarca
[......]
Tinogasta                     -28.066621  -67.566584        587  Catamarca
Charata                       -27.216286  -61.199996      18297  Chaco
Juan Jose Castelli            -25.949541  -60.616647       9421  Chaco
[......]

Se suministra un archivo binario a modo de ejemplo llamado �CIUDADES.DAT�. Tener en cuenta que este archivo
es s�lo una muestra. El programa debe funcionar para cualquier archivo con la misma estructura, aunque tenga
distinta cantidad de registros o provincias.
*/
#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<string.h>
#define ARCHIVOBINARIO "ciudades2.dat"
#define ARCHIVOTEXTO "ciudades.txt"
typedef struct sciudad {
	char nombre[40];
	float latitud;
	float longitud;
	int poblacion;
	char provincia[30];
}tciudad;

void generarArchivoBinario(char nombreTexto[], char nombreBinario[]);
void mostrarArchivoBinario(char nombreBinario[]);
void ordenarArchiboBinario(char nombreBinario[]);
void listadoCantidades(char nombreBinario[]);
void borradoLogico(char nombreBinario[]);
void generarArchivoTexto(char nombreBinario[], char nombreTexto[]);
void completarCadena(char cad[], int longitud);
void mostrarArchivoTexto(char nombreTexto[]);


int main(){

	//generarArchivoBinario(ARCHIVOTEXTO, ARCHIVOBINARIO);
	//mostrarArchivoBinario(ARCHIVOBINARIO);
	//ordenarArchiboBinario(ARCHIVOBINARIO);
	borradoLogico(ARCHIVOBINARIO);
	puts("\n");
	mostrarArchivoBinario(ARCHIVOBINARIO);
	puts("\n");
	generarArchivoTexto(ARCHIVOBINARIO, ARCHIVOTEXTO);
	//listadoCantidades(ARCHIVOBINARIO);
	puts("\n");
	puts("\n");
	mostrarArchivoTexto(ARCHIVOTEXTO);
	
	return 0;

}

void mostrarArchivoTexto(char nombreTexto[]){
	FILE * ft;
	char cad[256];
	ft = fopen(nombreTexto, "rt");
	if (ft == NULL){
		puts("No se puede leer el archivo!");
		return;
	}

	fgets(cad, 256, ft);

	while (!feof(ft)){
		puts(cad);
		fgets(cad, 256, ft);
	}
	fclose(ft);
}

void completarCadena(char cad[], int longitud){
	int x;
	x = strlen(cad);
	while (x < longitud){
		cad[x] = ' ';
		x++;
	}
	cad[x] = '\0';
}

void generarArchivoTexto(char nombreBinario[], char nombreTexto[]){
	FILE *fb, *ft;
	tciudad reg;
	fb = fopen(nombreBinario, "rb");
	ft = fopen(nombreTexto, "wt");
	if (fb == NULL){
		puts("No se puede leer el archivo!");
		return;
	}
	if (ft == NULL){
		puts("No se puede escribir el archivo!");
		return;
	}
	fread(&reg, sizeof(reg), 1, fb);
	while (!feof(fb)){
		
		if (reg.poblacion > 0){
			completarCadena(reg.nombre, 30);
			fprintf(ft, "%s  %f  %f  %10d  %s\n", reg.nombre, reg.latitud, reg.longitud, reg.poblacion, reg.provincia);
			
		}
		fread(&reg, sizeof(reg), 1, fb);
	}
	fclose(fb);
	fclose(ft);
	puts("Archivo de texto generado!!");
}

void borradoLogico(char nombreBinario[]){
	FILE * fb;
	tciudad reg;
	fb = fopen(nombreBinario, "rb+");
	
	if (fb == NULL){
		puts("No se puede leer el archivo!");
		return;
	}

	fread(&reg, sizeof(reg), 1, fb);
	while (!feof(fb)){
		if (reg.poblacion > 0 && reg.poblacion < 100000){
			printf("Borrando ciudad: %s con poblacion: %d\n", reg.nombre, reg.poblacion);
			reg.poblacion = reg.poblacion*-1;
			fseek(fb, -1 * (int)sizeof(reg), SEEK_CUR);
			fwrite(&reg, sizeof(reg), 1, fb);
			fseek(fb, 0, SEEK_CUR);
		}
		fread(&reg, sizeof(reg), 1, fb);
	}
	fclose(fb);
}


void listadoCantidades(char nombre[]){
	FILE * fb;
	tciudad reg;
	char provinciaAnterior[50];
	int totalProvincia, totalPais;
	fb = fopen(nombre, "rb");
	if (fb == NULL){
		puts("No se puede leer el archivo");
		return;
	}
	totalProvincia = 0;
	totalPais = 0;

	fread(&reg, sizeof(reg), 1, fb);
	strcpy(provinciaAnterior, reg.provincia);
	while (!feof(fb)){
		if (strcmp(reg.provincia, provinciaAnterior) == 0)
			totalProvincia = totalProvincia + reg.poblacion;
		else{
			printf("Total poblacion de %s: %d\n", provinciaAnterior, totalProvincia);
			totalProvincia = reg.poblacion;
			strcpy(provinciaAnterior, reg.provincia);
		}

		totalPais = totalPais + reg.poblacion;
		fread(&reg, sizeof(reg), 1, fb);
	}

	//La ultima provincia no fue impresa. La imprimimos manualmente y tambien el total del pais.
	printf("Total poblacion de %s: %d\n\n", reg.provincia, totalProvincia);
	printf("Total poblacion del pais: %d", totalPais);
	fclose(fb);
	puts("\n");
}
void ordenarArchiboBinario(char nombreBinario[]){
	FILE * fb;
	tciudad reg1, reg2;
	int cantReg, i=0, j, terminar=0;
	fb = fopen(nombreBinario, "rb+");
	if (fb == NULL){
		puts("No se puede leer el archivo");
		return;
	}
	fseek(fb, 0, SEEK_END);
	cantReg = ftell(fb) / sizeof(reg1);
	i = 1;
	puts("Ordenando archivo....");
	while (terminar == 0){
		terminar = 1;
		for (j = 0; j < cantReg - i; j++){
			fseek(fb, j*sizeof(tciudad), SEEK_SET);
			fread(&reg1, sizeof(reg1), 1, fb);
			fread(&reg2, sizeof(reg2), 1, fb);
			if (strcmp(reg1.nombre, reg2.nombre) > 0){
				fseek(fb, -2 * sizeof(reg1), SEEK_CUR);
				fwrite(&reg2, sizeof(reg2), 1, fb);
				fwrite(&reg1, sizeof(reg1), 1, fb);
				terminar = 0;
			}
		}
		i++;
	}
	fclose(fb);
	puts("Archivo ordenado!!");
}

void mostrarArchivoBinario(char nombreBinario[]){
	FILE *fb;
	tciudad reg;
	fb = fopen(nombreBinario, "rb");
	if (fb == NULL){
		puts("No se puede leer el archivo!!");
		return;
	}
	fread(&reg, sizeof(reg), 1, fb);
	while (!feof(fb)){
		puts(reg.nombre);
		printf("%f\n", reg.latitud);
		printf("%f\n", reg.longitud);
		printf("%d\n", reg.poblacion);
		puts(reg.provincia);
		
		fread(&reg, sizeof(reg), 1, fb);
		puts("\n");
	}
	fclose(fb);
}


void generarArchivoBinario(char nombreTexto[], char nombreBinario[]){
	FILE *fb, *ft;
	tciudad reg;
	char cadAux[80], c;
	int i = 0;
	ft = fopen(nombreTexto, "rt");
	if (ft == NULL){
		puts("No se puede leer el archivo de texto.");
		return;
	}
	fb = fopen(nombreBinario, "wb");
	if (fb == NULL){
		puts("No se puede grabar el archivo de texto.");
		return;
	}
	c = fgetc(ft);

	while (!feof(ft))
	{
		while (c != ';' && c != '\n'){
			cadAux[i] = c;
			i++;
			c = fgetc(ft);
		}
		cadAux[i] = '\0';
		strcpy(reg.nombre, cadAux);
		i = 0;
		c = fgetc(ft);

		while (c != ';' && c != '\n'){
			cadAux[i] = c;
			i++;
			c = fgetc(ft);
		}
		cadAux[i] = '\0';
		sscanf(cadAux, "%f", &reg.latitud);
		i = 0;
		c = fgetc(ft);

		while (c != ';' && c != '\n'){
			cadAux[i] = c;
			i++;
			c = fgetc(ft);
		}
		cadAux[i] = '\0';
		sscanf(cadAux, "%f", &reg.longitud);
		i = 0;
		c = fgetc(ft);

		while (c != ';' && c != '\n'){
			cadAux[i] = c;
			i++;
			c = fgetc(ft);
		}
		cadAux[i] = '\0';
		sscanf(cadAux, "%d", &reg.poblacion);
		i = 0;
		c = fgetc(ft);

		while (c != ';' && c != '\n'){
			cadAux[i] = c;
			i++;
			c = fgetc(ft);
		}
		cadAux[i] = '\0';
		strcpy(reg.provincia, cadAux);
		i = 0;
		fwrite(&reg, sizeof(reg), 1, fb);
		c = fgetc(ft);
	}
	fclose(ft);
	fclose(fb);
}

