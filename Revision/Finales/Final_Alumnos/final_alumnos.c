#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#define ARCHIVOTEXTO "alumnos.txt"
#define ARCHIVOBINARIO "alumnos.dat"

typedef struct talumno{

  char nombre [100];
  float prom;

}talumno;


int imprimirListado(char nombreTexto[],char nombreBinario[]);
int ordenarBinario(char nombreBinario[]);
int imprimirBinario (char nombreBinario[]);

int main()
{

  imprimirListado(ARCHIVOTEXTO,ARCHIVOBINARIO);
  ordenarBinario(ARCHIVOBINARIO);
  imprimirBinario(ARCHIVOBINARIO);
  return 0;

}


int imprimirListado(char nombreTexto[],char nombreBinario[])
{
    FILE *fb;
    FILE *ft;
    talumno reg;
    char c;
    char cad[80];
    int i = 0;
    float prom = 0;
    int sum = 0;
    int nota = 0;
    int acum = 0;

    fb = fopen (nombreBinario,"wb+");
    ft = fopen (nombreTexto, "rt");

    if(fb == NULL || ft == NULL)
      return 1;

    c = fgetc(ft);

    while(!feof(ft))
    {

        while(isdigit(c)==0)
        {
            cad[i] = c;
            i++;
            c = fgetc(ft);

        }

        cad[i]='\0';


        while(c=='\n')
          c = fgetc(ft);
        i=0;
        strcpy(reg.nombre,cad);

        while(isdigit(c)!=0 && !feof(ft))
        {

            sscanf(&c,"%d",&nota);
            c = fgetc(ft);
            if(isdigit(c)!=0 && c!='\n')
              {
                nota = nota + 9;
                c = fgetc(ft);

              }

            sum = sum + nota;
            i++;


            while(c=='\n')
              c = fgetc(ft);

        }



        prom = sum / i;

        sum = 0;
        i=0;

        reg.prom = prom;
        fwrite(&reg,sizeof(reg),1,fb);

    }



    fclose(fb);
    fclose(ft);

    return 0;


}

int ordenarBinario(char nombreBinario [])

{
    FILE *fb;
    talumno reg,reg2;
    int i=0;
    int j = 0;
    int cantReg = 0;
    int terminado = 0;

    fb = fopen(nombreBinario,"rb+");
    if(fb == NULL)
      return 1;

    fseek(fb,0,SEEK_END);
    cantReg = ftell(fb) / sizeof(reg);

    fseek(fb,0,SEEK_SET);

    i = 1;

    while(terminado == 0)
    {
          terminado = 1;

          for(j=0;j<cantReg-i;j++)
          {
            fread(&reg,sizeof(reg),1,fb);
            fread(&reg2,sizeof(reg2),1,fb);
              if(reg.prom<reg2.prom)
                {
                    fseek(fb,-2*sizeof(reg),SEEK_CUR);
                    fwrite(&reg2,sizeof(reg2),1,fb);
                    fwrite(&reg,sizeof(reg),1,fb);
                    terminado = 0;

                }



          }

        i++;

    }
    fclose(fb);
    return 0;
}


int imprimirBinario(char nombreBinario[])
{

  FILE *fb;
  talumno reg;
  fb = fopen(nombreBinario,"rb+");
  if(fb == NULL)
    return 1;

  fread(&reg,sizeof(reg),1,fb);

  while(!feof(fb))
  {
      printf("\nNombre: %s",reg.nombre);
      printf("Prom: %f",reg.prom);
      fread(&reg,sizeof(reg),1,fb);
  }

  fclose(fb);
  return 0;

}
