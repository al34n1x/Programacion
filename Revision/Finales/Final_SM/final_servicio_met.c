#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
#define ARCHIVOBINARIO "sm.dat"


typedef struct sdatos{

  int day;
  int month;
  int year;
  float temp;

}tdatos;


int generarArchivoBinario(char nombreBinario[]);
int imprimirArchivoBinario(char nombreBinario[]);
int ordenarArchiboBinario(char nombreBinario[]);
int imprimirTemperatura(char nombreBinario[]);
int imprimirBorradoLogico(char nombreBinario[]);


int main()
{
    srand(time(NULL));
    generarArchivoBinario(ARCHIVOBINARIO);
    ordenarArchiboBinario(ARCHIVOBINARIO);
    imprimirArchivoBinario(ARCHIVOBINARIO);
    imprimirTemperatura(ARCHIVOBINARIO);
    imprimirBorradoLogico(ARCHIVOBINARIO);

    return 0;


}


int generarArchivoBinario(char nombreBinario[])
{

    FILE *arch;
    int i = 0;
    int j = 0;
    tdatos reg;
    char sel;

    arch=fopen(nombreBinario,"wb+");
    if(arch==NULL)
    {
        printf("Archivo no puede abrirse");
        return 1;

    }


    for (i=0;i<100;i++)
      {

                  reg.day = rand()%(31-1+1)+1;
                  reg.month = rand()%(12-1+1)+1;
                  reg.year = rand()%(2016-1990+1)+1990;
                  reg.temp = rand()%(50+20+1)-20;
                  fwrite(&reg,sizeof(reg),1,arch);
                  //while(getchar()!='\n')
                  //  ;
                  //printf("Ingrese Selección: ");
                  //scanf("%c",&sel);
                  //fflush(stdin);

    }

      fclose(arch);

      return 0;

}


int ordenarArchiboBinario(char nombreBinario[])
{

  FILE *arch;
  tdatos reg;
  tdatos reg2;
  int i = 0;
  int j = 0;
  int terminado = 0;
  int cantReg = 0;

  arch = fopen(nombreBinario,"rb+");
  if(arch==NULL)
    {
      printf("No se pudo abrir archivo");
      return 1;
    }

  i=1;
  fseek(arch,0,SEEK_END);
  cantReg = ftell(arch) / sizeof(tdatos);

  while(terminado == 0)
  {
      terminado = 1;
      for (j=0; j<cantReg-i; j++)
      {
          fseek(arch,j*sizeof(reg),SEEK_SET);
          fread(&reg,sizeof(reg),1,arch);
          fread(&reg2,sizeof(reg),1,arch);

          if(reg.year>reg2.year)
          {
            fseek(arch,-2*sizeof(reg),SEEK_CUR);
            fwrite(&reg2,sizeof(reg),1,arch);
            fwrite(&reg,sizeof(reg),1,arch);
            terminado = 0;

          }

          if(reg.month>reg2.month && reg.year == reg2.year)
          {
            fseek(arch,-2*sizeof(reg),SEEK_CUR);
            fwrite(&reg2,sizeof(reg),1,arch);
            fwrite(&reg,sizeof(reg),1,arch);
            terminado = 0;

          }


          if (reg.day > reg2.day && reg.month == reg2.month && reg.year == reg2.year)
            {

              fseek(arch,-2*sizeof(reg),SEEK_CUR);
              fwrite(&reg2,sizeof(reg),1,arch);
              fwrite(&reg,sizeof(reg),1,arch);
              terminado = 0;

            }


      }

      i++;

  }

  fclose(arch);
  return 0;

}

int imprimirArchivoBinario(char nombreBinario[])
{
    FILE *arch;
    tdatos reg;

    arch=fopen(nombreBinario,"rb+");
    if(arch==NULL)
    {
        printf("No se pudo abrir el archivo");
        return 1;

    }

    fread(&reg, sizeof(reg),1,arch);

    while(!feof(arch))
    {
        printf("\n%d",reg.day);
        printf("%10d",reg.month );
        printf("%10d",reg.year );
        printf("%10f",reg.temp );
        fread(&reg, sizeof(reg),1,arch);

    }
    printf("\n");
  fclose(arch);
  return 0;
}

int imprimirTemperatura(char nombreBinario[])
{

    FILE *arch;
    tdatos reg;
    float max=0;
    float min=0;
    float prom=0;
    int dia_ant=0;


    arch=fopen(nombreBinario,"rb+");
    if(arch==NULL)
    {
        printf("No se puede abrir archivo");
        return 1;

    }

    fread(&reg,sizeof(reg),1,arch);
    dia_ant = reg.day;


    while(!feof(arch))
    {

        while(dia_ant == reg.day)
        {
            if(reg.temp > max)
              max = reg.temp;

            if(reg.temp < min)
              min = reg.temp;

            if(reg.temp != min && reg.temp!=max)
              {
                reg.temp = -100;
                fseek(arch,-1*sizeof(reg),SEEK_CUR);
                fwrite(&reg,sizeof(reg),1,arch);
              }

            fread(&reg,sizeof(reg),1,arch);

        }


        prom=(max+min)/2;
        printf("\n Maxima para día fecha %d %d %d: %f", reg.day, reg.month, reg.year, max);
        printf("\n Minima para día fecha %d %d %d: %f", reg.day, reg.month, reg.year, min);
        printf("\n Prom para día fecha %d %d %d: %f", reg.day, reg.month, reg.year, prom);
        dia_ant = reg.day;
        max = reg.temp;
        min = reg.temp;
        fread(&reg,sizeof(reg),1,arch);


    }
    /*ultimo registro*/
    printf("\n Maxima para día fecha %d %d %d: %f", reg.day, reg.month, reg.year, max);
    printf("\n Minima para día fecha %d %d %d: %f", reg.day, reg.month, reg.year, min);
    printf("\n Prom para día fecha %d %d %d: %f", reg.day, reg.month, reg.year, prom);
    printf("\n");
    fclose(arch);
    return 0;

}


int imprimirBorradoLogico(char nombreBinario[])
{
  FILE *arch;
  tdatos reg;

  arch = fopen(nombreBinario,"rb+");
  if (arch==NULL)
  {
    printf("No se puede abrir el archivo");
    return 1;
  }

  fread(&reg, sizeof(reg), 1, arch);

  while(!feof(arch))
  {

    if(reg.temp != -100)
      {
          printf("\nFecha %d %d %d Temp calculo: %4.4f",reg.day,reg.month,reg.year, reg.temp );
      }

    fread(&reg, sizeof(reg), 1, arch);

  }

  printf("\n");
  fclose (arch);
  return 0;

}
