#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

typedef struct snodo{

  int valor;
  struct snodo *sig;

}tnodo;

typedef tnodo *tpuntero;

int insertaralprincipio(tpuntero *cabeza);
int imprimirLista(tpuntero cabeza);

int main()

{
  srand(time(NULL));
  tpuntero cabeza;
  int e;
  int i;
  cabeza=NULL;
  for (i=0;i<10;i++)
  {
    insertaralprincipio(&cabeza);

  }

  imprimirLista(cabeza);

return 0;

}

int insertaralprincipio(tpuntero *cabeza)
{


  tpuntero nuevo;
  nuevo = (tpuntero)malloc(sizeof(tnodo));
  nuevo -> valor = rand()%(118-75+1)+75;
  nuevo -> sig = *cabeza;
  *cabeza = nuevo;

  return 0;
}


int imprimirLista(tpuntero cabeza)
{
  while(cabeza!=NULL)
  {
      printf("%8d",cabeza->valor);
      cabeza = cabeza -> sig;

  }

  return 0;
}
