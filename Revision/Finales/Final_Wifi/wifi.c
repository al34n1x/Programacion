/*Se tiene un archivo de texto con los nombres y ubicaciones de los puntos WiFi de acceso
p�blico y gratuito de la Ciudad de Buenos Aires. Este archivo es de los denominados CSV
(Comma Separated Values, valores separados por comas), es decir que cada l�nea contiene
una serie de campos que est�n separados mediante puntos y comas, ya que en la regi�n la
coma se utiliza como separador decimal y podr�an producirse confusiones.
Los campos contenidos en cada l�nea son:

	nombre;categoria;domicilio;barrio;comuna;longitud;latitud

	El campo nombre tiene una longitud m�xima de 80 caracteres, la categor�a 50 caracteres,
el domicilio 70 caracteres y el barrio 40 caracteres. El campo comuna es de tipo entero y
los campos longitud y latitud son de tipo float.

Se solicita realizar un programa para realizar las siguientes tareas:
	
	1. Generar un archivo binario a partir del archivo de texto, donde cada registro contenga
todos los campos incluidos en cada l�nea del archivo original.
	
	2. Ordenar el archivo binario por barrio, en orden alfab�tico ascendente, utilizando
cualquier m�todo de ordenamiento estudiado. Recuerde que los archivos no pueden
cargarse completos en memoria.
	
	3. Imprimir el archivo binario obtenido.
	
	4. Imprimir un listado indicando cu�ntos puntos de acceso existen en cada barrio de la
Ciudad, ignorando las may�sculas y min�sculas.
Ejemplo de listado de puntos de acceso por barrios:

ALMAGRO 5
BALVANERA 12
[��]
VILLA SOLDATI 13
VILLA URQUIZA 4

Se suministra un archivo de texto a modo de ejemplo llamado �WIFI.TXT�. Tener en cuenta
que este archivo es s�lo una muestra. Los barrios contenidos en �l pueden cambiar debido
a subdivisiones o unificaciones, y diariamente se agregan nuevos puntos de acceso p�blico
distribuidos por la ciudad.*/

#include<stdio.h>
#include<string.h>

#define _CRT_SECURE_NO_WARNINGS
#define ARCHIVOTEXTO "wifi.txt"
#define ARCHIVOBINARIO "wifi.dat"

typedef struct spunto{
	char nombre[40], categoria[40], domicilio[40], barrio[40]; 
	int comuna;
	float longitud; 
	float latitud;
}tpunto;

void generarBin(char nombreTexto[], char nombreBinario[]);
void leerBinario(char nombreBinario[]);
void ordenarArchivo(char nombreBinario[]);
void puntosDeAcceso(char nombreBinario[]);
void borradoLogico(char nombreBinario[]);
void generarTex(char nombreBinario[], char nombreTexto[]);
void rellenarEspacio(char cad[], int longitud);
void leerTexto(char nombreTexto[]);

int main(){
	//generarBin(ARCHIVOTEXTO, ARCHIVOBINARIO);
	leerBinario(ARCHIVOBINARIO);
	//ordenarArchivo(ARCHIVOBINARIO);
	puts("\n");
	//leerBinario(ARCHIVOBINARIO);
	puntosDeAcceso(ARCHIVOBINARIO);
	puts("\n");
	//leerBinario(ARCHIVOBINARIO);
	//generarTex(ARCHIVOBINARIO, ARCHIVOTEXTO);

	//leerTexto(ARCHIVOTEXTO);
	//borradoLogico(ARCHIVOBINARIO);
	return 0;
}

void leerTexto(char nombreTexto[]){
	FILE *ft;
	char c;
	ft = fopen(nombreTexto, "rt");
	if (ft == NULL){
		puts("No se puede leer el archivo!");
		return;
	}
	c = fgetc(ft); 
	while (!feof(ft)){
		printf("%c", c);
		c = fgetc(ft);
	}
	fclose(ft);
}

void rellenarEspacio(char cad[], int longitud){
	int x;
	x = strlen(cad);
	while (x < longitud){
		cad[x] = ' ';
		x++;
	}
	cad[x] = '\0';
}


void generarTex(char nombreBinario[], char nombreTexto[]){
	FILE*fb, *ft;
	tpunto reg;

	ft = fopen(nombreTexto, "wt");
	fb = fopen(nombreBinario, "rb");
	if (ft == NULL){
		puts("No se puede escribir el archivo");
		return;
	}
	if (fb == NULL){
		puts("No se puede abrir el archivo");
		return;
	}

	fread(&reg, sizeof(reg), 1, fb);
	while (!feof(fb)){
		if (reg.comuna > 0){
			rellenarEspacio(reg.nombre, 20);
			fprintf(ft, "%s  %s  %s  %s %d  %f  %f\n\n", reg.nombre, reg.categoria, reg.domicilio, reg.barrio, reg.comuna, reg.latitud, reg.longitud);
		}
		fread(&reg, sizeof(reg), 1, fb);
	}
	fclose(fb);
	fclose(ft);
}

void borradoLogico(char nombreBinario[]){
	FILE*fb;
	tpunto reg;
	fb = fopen(nombreBinario, "rb+");
	if (fb == NULL){
		puts("No se puede abrir el archivo!");
		return;
	}
	fread(&reg, sizeof(reg), 1, fb);
	while (!feof(fb)){
		if (reg.comuna > 0 && reg.comuna < 5){
			printf("Borrando Logicamente %s con comuna %d\n", reg.nombre, reg.comuna);
			reg.comuna = reg.comuna * -1;
			fseek(fb, -1 * (int)sizeof(reg), SEEK_CUR);
			fwrite(&reg, sizeof(reg), 1, fb);
			fseek(fb, 0, SEEK_CUR);
		}
		fread(&reg, sizeof(reg), 1, fb);
	}
	fclose(fb);
}

void puntosDeAcceso(char nombreBinario[]){
	FILE * fb;
	tpunto reg;
	int cantidad = 0;
	char cad[50];
	fb = fopen(nombreBinario, "rb");
	if (fb == NULL){
		puts("No se puede leer el archivo!");
		return;
	}
	fread(&reg, sizeof(reg), 1, fb);
	strcpy(cad, reg.barrio);
	while (!feof(fb)){
		if (strcmp(reg.barrio, cad) == 0)
			cantidad = cantidad + 1;
		else{
			printf("%-20s %d\n", cad, cantidad);
			cantidad = 1;
			strcpy(cad, reg.barrio);
		}
		fread(&reg, sizeof(reg), 1, fb);

	}
	printf("%-20s %d\n", reg.barrio, cantidad);
	fclose(fb);
}

void ordenarArchivo(char nombreBinario[]){
	FILE * fb;
	int cantReg, terminar = 0, j, i;
	tpunto reg1, reg2;
	fb = fopen(nombreBinario, "rb+");
	if (fb == NULL){
		puts("No se puede abrir el archivo!");
		return;
	}
	fseek(fb, 0, SEEK_END);
	cantReg = ftell(fb) / sizeof(reg1);
	i = 0;
	puts("ordenando archivo....");
	while (terminar == 0){
		terminar = 1;
		for (j = 0; j < cantReg - i; j++){
			fseek(fb, j*sizeof(reg1), SEEK_SET);
			fread(&reg1, sizeof(reg1), 1, fb);
			fread(&reg2, sizeof(reg2), 1, fb);
			if (strcmp(reg1.barrio, reg2.barrio)>0){
				fseek(fb, -2 * (int)sizeof(reg1), SEEK_CUR);
				fwrite(&reg2, sizeof(reg2), 1, fb);
				fwrite(&reg1, sizeof(reg1), 1, fb);
				terminar = 0;
			}
		}
		i++;
	}
	fclose(fb);
	puts("Archivo ordenado!");
}

void leerBinario(char nombreBinario[]){
	FILE * fb;
	tpunto reg;
	fb = fopen(nombreBinario, "rb");
	if (fb == NULL){
		puts("No se puede leer el archivo!");
		return;
	}
	fread(&reg, sizeof(reg), 1, fb);
	while (!feof(fb)){
		puts(reg.nombre);
		puts(reg.categoria);
		puts(reg.domicilio);
		puts(reg.barrio);
		printf("%d\n", reg.comuna);
		printf("%f\n", reg.latitud);
		printf("%f\n", reg.longitud);
		puts("\n");
		fread(&reg, sizeof(reg), 1, fb);
	}
	fclose(fb);
}

void generarBin(char nombreTexto[], char nombreBinario[]){
	FILE*fb, *ft;
	tpunto reg;
	char c, cadAux[80];
	int i = 0;
	ft = fopen(nombreTexto, "rt");
	fb = fopen(nombreBinario, "wb");
	if (ft == NULL){
		puts("No se puede abrir el archivo");
		return;
	}
	if (fb == NULL){
		puts("No se puede escribir el archivo");
		return;
	}

	c = fgetc(ft);
	while (!feof(ft)){
		while (c != ';' && c != '\n'){
			cadAux[i] = c;
			i++;
			c = fgetc(ft);
		}
		cadAux[i] = '\0';
		strcpy(reg.nombre, cadAux);
		i = 0;
		c = fgetc(ft);


		while (c != ';' && c != '\n'){
			cadAux[i] = c;
			i++;
			c = fgetc(ft);
		}
		cadAux[i] = '\0';
		strcpy(reg.categoria, cadAux);
		i = 0;
		c = fgetc(ft);

		while (c != ';' && c != '\n'){
			cadAux[i] = c;
			i++;
			c = fgetc(ft);
		}
		cadAux[i] = '\0';
		strcpy(reg.domicilio, cadAux);
		i = 0;
		c = fgetc(ft);

		while (c != ';' && c != '\n'){
			cadAux[i] = c;
			i++;
			c = fgetc(ft);
		}
		cadAux[i] = '\0';
		strcpy(reg.barrio, cadAux);
		i = 0;
		c = fgetc(ft);

		while (c != ';' && c != '\n'){
			cadAux[i] = c;
			i++;
			c = fgetc(ft);
		}
		cadAux[i] = '\0';
		sscanf(cadAux, "%d", &reg.comuna);
		i = 0;
		c = fgetc(ft);

		while (c != ';' && c != '\n'){
			cadAux[i] = c;
			i++;
			c = fgetc(ft);
		}
		cadAux[i] = '\0';
		sscanf(cadAux, "%f", &reg.longitud);
		i = 0;
		c = fgetc(ft);

		while (c != ';' && c != '\n'){
			cadAux[i] = c;
			i++;
			c = fgetc(ft);
		}
		cadAux[i] = '\0';
		sscanf(cadAux, "%f", &reg.latitud);
		i = 0;


		fwrite(&reg, sizeof(reg), 1, fb);
		c = fgetc(ft);

	}

	fclose(ft);
	fclose(fb);
}