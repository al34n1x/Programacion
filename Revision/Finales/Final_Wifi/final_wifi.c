/*

Se solicita realizar un programa para realizar las siguientes tareas:

1. Generar un archivo binario a partir del archivo de texto, donde cada registro con-
tenga todos los campos incluidos en cada línea del archivo original.

2. Ordenar el archivo binario por barrio, en orden alfabético ascendente, utilizando
cualquier método de ordenamiento estudiado. Recuerde que los archivos no pueden
cargarse completos en memoria.

3. Imprimir el archivo binario obtenido.

4. Imprimir un listado indicando cuántos puntos de acceso existen en cada barrio de la

Ciudad, ignorando las mayúsculas y minúsculas.

*/


#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
#define ARCHIVOTEXTO "wifi.txt"
#define ARCHIVOBINARIO "wifi.dat"

typedef struct swifi{

  char nombre[100];
  char categoria[100];
  char domicilio[100];
  char barrio[100];
  int comuna;
  float longitud;
  float latitud;
} twifi;


int generarArchivoBinario(char nombreTexto[], char nombreBinario[]);
int ordenarBinario(char nombreBinario[]);
int imprimirBinario(char nombreBinario[]);
int imprimirAP(char nombreBinario[]);


int main()
{

  generarArchivoBinario(ARCHIVOTEXTO,ARCHIVOBINARIO);
  ordenarBinario(ARCHIVOBINARIO);
  imprimirBinario(ARCHIVOBINARIO);
  imprimirAP(ARCHIVOBINARIO);

  return 0;
}

int generarArchivoBinario(char nombreTexto[], char nombreBinario[])
{

  FILE *ft;
  FILE *fb;
  twifi reg;
  char c;
  char cad[100];
  int i = 0;


  ft = fopen(nombreTexto,"rt");
  fb = fopen(nombreBinario,"wb+");

  if(ft == NULL || fb == NULL)
  {
    printf("No se pudo abrir archivo");
    return 1;
  }

  c = fgetc(ft);

  while(!feof(ft))
  {
      while(c!=';' && c!='\n')
      {
          cad[i] = c;
          c = fgetc(ft);
          i++;

      }

      cad[i]='\0';
      i=0;
      strcpy(reg.nombre,cad);
      c = fgetc(ft);

      while(c!=';' && c!='\n')
      {
          cad[i] = c;
          c = fgetc(ft);
          i++;

      }
      cad[i]='\0';
      i=0;
      strcpy(reg.categoria,cad);
      c = fgetc(ft);

      while(c!=';' && c!='\n')
      {
          cad[i] = c;
          c = fgetc(ft);
          i++;

      }
      cad[i]='\0';
      i=0;
      strcpy(reg.domicilio,cad);
      c = fgetc(ft);

      while(c!=';' && c!='\n')
      {
          cad[i] = c;
          c = fgetc(ft);
          i++;

      }
      cad[i]='\0';
      i=0;
      strcpy(reg.barrio,cad);
      c = fgetc(ft);

      while(c!=';' && c!='\n')
      {
          cad[i] = c;
          c = fgetc(ft);
          i++;

      }
      cad[i]='\0';
      i=0;
      sscanf(cad,"%d",&reg.comuna);
      c = fgetc(ft);

      while(c!=';' && c!='\n')
      {
          cad[i] = c;
          c = fgetc(ft);
          i++;

      }
      cad[i]='\0';
      i=0;
      sscanf(cad,"%4f",&reg.longitud);
      c = fgetc(ft);

      while(c!=';' && c!='\n')
      {
          cad[i] = c;
          c = fgetc(ft);
          i++;

      }
      cad[i]='\0';
      i=0;
      sscanf(cad,"%4f",&reg.latitud);
      c = fgetc(ft);

      fwrite(&reg,sizeof(reg),1,fb);

  }

  fclose(fb);
  fclose(ft);
  return 0;

}

int ordenarBinario(char nombreBinario[])
{
  FILE *fb;
  twifi reg;
  twifi reg2;
  int i = 0;
  int j = 0;
  int h = 0;
  int cantReg = 0;
  int terminado = 0;


  fb=fopen(nombreBinario,"rb+");
  if (fb == NULL)
    {
        return 1;
    }

  fseek(fb,0,SEEK_END);
  cantReg = ftell(fb)/sizeof(reg);

  i = 1;
  while(terminado == 0)
  {
      terminado = 1;
      for (j=0;j<cantReg-i;j++)
      {
        fseek(fb,j*sizeof(reg),SEEK_SET);
        fread(&reg,sizeof(reg),1,fb);
        fread(&reg2,sizeof(reg),1,fb);

        if(strcmp(reg.barrio,reg2.barrio)>0)
        {
            fseek(fb,-2*sizeof(reg),SEEK_CUR);
            fwrite(&reg2,sizeof(reg),1,fb);
            fwrite(&reg,sizeof(reg),1,fb);
            terminado = 0;

        }


      }

      i++;

  }

  fclose(fb);
  return 0;


}

int imprimirBinario(char nombreBinario[])
{
  FILE *fb;
  twifi reg;

  fb = fopen(nombreBinario,"rb+");
  if (fb == NULL)
  {
    return 1;
  }

  fread(&reg,sizeof(reg),1,fb);

  while(!feof(fb))
  {

      printf("\n%s",reg.nombre);
      printf("\t%s",reg.categoria);
      printf("\t%s",reg.domicilio);
      printf("\t%s",reg.barrio);
      printf("\t%d",reg.comuna);
      printf("\t%4.4f",reg.longitud);
      printf("\t%4.4f",reg.latitud);

      fread(&reg,sizeof(reg),1,fb);


  }

  fclose(fb);
  return 0;

}


int imprimirAP(char nombreBinario[])
{
    FILE *fb;
    twifi reg;
    int cant = 0;
    char cad[100];
    char cad2[100];
    int i = 0;


    fb = fopen(nombreBinario,"rb+");

    if (fb == NULL)
      return 1;


    fread(&reg,sizeof(reg),1,fb);
    strcpy(cad,reg.barrio);
    fread(&reg,sizeof(reg),1,fb);
    strcpy(cad2,reg.barrio);

    for(i=0;i<100;i++)
      {
        cad[i]=tolower(cad[i]);
        cad2[i]=tolower(cad2[i]);
     }
    while(!feof(fb))
    {

      while(strcmp(cad,cad2)==0)
      {

          cant = cant + 1;
          fread(&reg,sizeof(reg),1,fb);
          strcpy(cad,reg.barrio);
          fread(&reg,sizeof(reg),1,fb);
          strcpy(cad2,reg.barrio);
          for(i=0;i<100;i++)
            {
              cad[i]=tolower(cad[i]);
              cad2[i]=tolower(cad2[i]);
           }

      }

      printf("\nBarrio %s: %d",cad,cant);
      cant = 1;
      fread(&reg,sizeof(reg),1,fb);
      strcpy(cad,reg.barrio);
      fread(&reg,sizeof(reg),1,fb);
      strcpy(cad2,reg.barrio);
      for(i=0;i<100;i++)
        {
          cad[i]=tolower(cad[i]);
          cad2[i]=tolower(cad2[i]);
       }


    }
    /*Ultimo Registro*/
    printf("\nBarrio %s: %d",cad,cant);
    cant = 1;

    printf("\n");
    fclose(fb);
    return 0;

}
