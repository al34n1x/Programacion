/*

Realizar una función que permita eliminar un valor de un arreglo. Se sabe que todos los
elementos válidos del vector se almacenan en forma consecutiva, y que los elementos no
utilizados se inicializan con -1.

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#define MAX 10

void eliminar(int vec[MAX], int res[MAX], int valor);
void imprimir (int vec[MAX]);


int main ()
{
  int vec[]={1,0,0,3,4,-1,-1,-1,-1,-1};
  int res[]={-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};
  int valor = 0;

  printf("Ingrese valor a eliminar: ");
  scanf("%d", &valor);

  eliminar (vec, res, valor);
  imprimir (res);

  return 0;
}


void eliminar (int *v, int *res, int valor)
{

  int i = 0;
  int j = 0;
  int r = 0;

  for (i=0;i<MAX;i++)
  {
      if (v[i] == valor)
      {
          v[i] = -1;

      }


  }



  while (j<MAX)
  {

    if (v[j]!=-1)
      {
          res[r] = v[j];
          r++;
      }

    j++;

  }

}


void imprimir (int *v)
{

  int i=0;
  for (i=0;i<MAX;i++)
  {
    printf("%d\n",v[i]);

  }


}
