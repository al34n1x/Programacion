/*Hacer una función que cargue una matriz con números al azar entre 0 y 999, la cual representa la
lluvia mensual caída del año 1995 al 2008. Las filas representan los años y las columnas
correponden a los 12 meses del año.*/

/*

a) Almacenar en un vector la lluvia promedio caida en cada uno de los meses

b) Almacenar en un vector la lluvia promedio caida durante cada uno de los años

c) Mostrar los resultados obtenidos en los puntos a y b

*/



#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <time.h>
#define MAXC 12
#define MAXF 13

int carga(int mat[MAXF][MAXC]);
void imprimir(int mat[MAXF][MAXC]);
void imprimir_mes(float vec[MAXC]);
void imprimir_year(float vec[MAXF]);
void prom_mes(int mat[MAXF][MAXC], float vec_mes[MAXC]);
void prom_year(int mat[MAXF][MAXC], float vec_year[MAXF]);

int main()
{
  int mat[MAXC][MAXF];
  float vec_mes[MAXC];
  float vec_year[MAXF];
  srand(time(NULL));
  carga(mat);
  imprimir(mat);
  prom_mes(mat,vec_mes);
  prom_year(mat,vec_year);
  imprimir_mes(vec_mes);
  imprimir_year(vec_year);


  return 0;
}

int carga(int m[MAXF][MAXC])
{

  int i=0;
  int j=0;

  for (i=0;i<MAXF;i++)
    for (j=0;j<MAXC;j++)
      {

        m[i][j] = rand()%(999+1);  //rand()%(vf-vi+1)-vi


      }
  return 0;

}

void imprimir(int m[MAXF][MAXC])
{

  int i=0;
  int j=0;

  for (i=0;i<MAXF;i++)
    {
    for (j=0;j<MAXC;j++)
      {
          printf("%4d",m[i][j]);
      }

      printf("\n");

    }

}

void prom_mes(int mat[MAXF][MAXC], float *v)
{
    int i=0;
    int j=0;
    int suma = 0;

    for (i=0;i<MAXC;i++)
    {
      for (j=0;j<MAXF;j++)

        {
          suma = suma + mat[i][j];

        }

        v[i]=(float)suma/MAXC;
        suma = 0;
    }

}

void prom_year(int mat[MAXF][MAXC], float *v)
{
    int i=0;
    int j=0;
    int suma = 0;

    for (i=0;i<MAXF;i++)
    {
      for (j=0;j<MAXC;j++)

        {
          suma = suma + mat[i][j];

        }

        v[i]=(float)suma/MAXF;
        suma = 0;
    }

}

void imprimir_mes(float *v)
{

  int j=0;


    for (j=0;j<MAXC;j++)
      {
          printf("%4f\n",v[j]);
      }

      printf("\n");

}


void imprimir_year(float *v)
{

  int j=0;


    for (j=0;j<MAXF;j++)
      {
          printf("%4f\n",v[j]);
      }

      printf("\n");

}
