/*Realizar una función que devuelva la posición en que se haya un valor dado mediante el método

de búsqueda binaria. En caso que no lo encuentre devolver*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#define MAX 10


int busqueda (int v[MAX]);
int inicializar (int *v);
int carga (int *v);
void imprimir (int *v);


int main ()
{

  int vec[10];

  inicializar(vec);
  carga(vec);
  imprimir(vec);
  printf("la Posición del valor a buscar es: %d\n", busqueda(vec));

  return 0;
}


int inicializar (int *v)
{

  int i = 0;

  for (i=0; i<MAX; i++)
    {
        *v=0;

    }
  return 0;

}


int carga (int *v)

{

  int i = 0;
  for (i=0; i<MAX; i++)
    {

      printf("Ingrese Valor: ");
      scanf("%d",(v+i));
      fflush(stdin);

    }

  return 0;

}


void imprimir(int *v)
{

  int i = 0;
  for (i=0;i<MAX;i++)
    {
      printf("%d\n", *(v+i));
    }

}


int busqueda(int *v)
{
  int valor=0;
  int i=-1;
  int sup = (MAX-1);
  int inf = 0;
  int centro = 0;
  printf("Ingrese valor a buscar: ");
  scanf("%d", &valor);

  while (inf<=sup )
  {
        centro = (sup-inf)/2;
        if(valor == *(v+centro))
          {
          return centro;
          }

        else
          {
             if(valor < *(v+centro))
                {
                  sup = sup - 1;

                }
            else
                {
                inf = inf + 1;

                }

          }

  }

  return i;

}
