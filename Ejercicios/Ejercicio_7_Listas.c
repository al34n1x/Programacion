#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>


typedef struct snodo{

  int valor;
  struct snodo *ant, *sig;

}tnodo;

typedef tnodo *tpuntero;

int agregar(tpuntero *cabeza, int valor);
int imprimir(tpuntero *cabeza);

int main()
{
  tpuntero cabeza;
  int valor = 0;
  cabeza = NULL;

  printf("Ingrese Valor, -1 para salir: ");
  scanf("%d", &valor);

  while(valor!=-1)
  {
      agregar(&cabeza, valor);
      printf("Ingrese Valor, -1 para salir: ");
      scanf("%d", &valor);

  }

  imprimir(&cabeza);

  return 0;


}


int agregar (tpuntero *cabeza, int valor)
{
    tpuntero nuevo, anterior, actual;
    anterior = (tpuntero)malloc(sizeof(tnodo));
    actual = (tpuntero)malloc(sizeof(tnodo));
    nuevo = (tpuntero)malloc(sizeof(tnodo));
    anterior = NULL;
    actual = *cabeza;

    while((actual!=NULL) && (actual->valor < valor))
    {
        anterior = actual;
        actual = actual->sig;

    }

    nuevo -> valor = valor;
    nuevo -> sig = actual;
    nuevo -> ant = anterior;

    if(anterior != NULL)
      anterior->sig = nuevo;
    else
        *cabeza = nuevo;
    if(actual != NULL)
        actual->ant = nuevo;


    return 0;
}


int imprimir (tpuntero *cabeza)
{
    tpuntero nuevo;
    nuevo = (tpuntero)malloc(sizeof(tnodo));
    nuevo = *cabeza;

    while(nuevo->sig!=NULL)
    {
        nuevo = nuevo->sig;

    }

    while(nuevo!=NULL)
    {
        printf("%d\n", nuevo->valor);
        nuevo = nuevo ->ant;

    }

  return 0;

}
