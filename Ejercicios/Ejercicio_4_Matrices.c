/*Dada una matriz de 5x5 de números enteros, ordenar
en forma ascendente cada una de sus filas.*/

#include <stdio.h>

#define MAXF 3
#define MAXC 3

int main()

{
  int f = 0;
  int c = 0;
  int mat [MAXF][MAXC] = {0};
  int flag = 0;
  int aux = 0;


  for (f=0; f<MAXF; f++)
    for (c=0; c<MAXC; c++)
      {
          printf("Ingrese valor para fila %d, Columna %d: ", f+1, c+1);
          scanf("%d",&mat[f][c]);
      }

  for(f=0; f<MAXF; f++)
      {
        printf("\n");
        for(c=0; c<MAXC; c++)
          printf("%4d", mat[f][c]);
      }

  printf("\n");


  while(flag == 0)
  {
      flag = 1;

      for(c=0; c<MAXC; c++)
      {  for(f=0; f<MAXF; f++)
        {

            if(mat[f][c]>mat[f+1][c])
              {
                aux = mat[f][c];
                mat[f][c]=mat[f+1][c];
                mat[f+1][c] = aux;
                flag = 0;
              }

        }
      }

  }

  for(f=0; f<MAXF; f++)
        {
            printf("\n");
            for(c=0; c<MAXC; c++)
              printf("%4d", mat[f][c]);
        }

        printf("\n");

    return 0;


}
