/* Realizar una función que permita eliminar un valor de un arreglo. Se sabe que todos los

elementos válidos del vector se almacenan en forma consecutiva, y que los elementos no

utilizados se inicializan con -1. */

#include <stdio.h>
#define max 10

int eliminar_valor(int valor, int *ptr);


int main()

{

    int vec[max]={0};
    int i = 0;
    int e = 0;
    int a = 0;

    for (i=0; i < max; i++)
      vec[i]=-1;

      i = 0;
    do{
        printf("Ingrese valor para la posición %d: ", i+1);
        scanf("%d", &a);
        vec[i]=a;
        i++;
    }while((a!=-1) && (i<max));

    printf("Ingrese Valor a Eliminar:");
    scanf("%d", &e );

    eliminar_valor(e,&vec[0]);

    for(i=0; i<max;i++)
      printf("%d\n", vec[i]);


      return 0;

}


int eliminar_valor (int valor, int *ptr)
{

  int i = 0;
  int aux [max]= {0};
  int j = 0;
  int *p;

  p = ptr;


  for (i=0; i<max; i++)
    {
      ptr = ptr + i;
      if(*ptr == valor)
        *ptr = -1;
    }

  ptr = p;

  for (i=0; i<max; i++)
    {
        aux[i]=*ptr;
        *ptr = -1;
        ptr = ptr + 1;
    }

  ptr = p;

  for (i=0; i<max; i++)

{
  if (aux[i]!= -1)
    {
      *ptr = aux[i];
      ptr = ptr + 1;
    }

}




return 0;


}
