/*Hacer un programa que invoque y pruebe las siguientes funciones:

 grabarMatriz() permite grabar una matriz, tal que en un renglón se encuentren los valores
reales de toda una fila
 leerMatriz() permite cargar una matriz desde el archivo generado por la función anterior*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
#define MAXF 3
#define MAXC 3

int grabarMatriz();
int leerMatriz();

int main()
{

  srand(time(NULL));
  grabarMatriz();
  leerMatriz();

  return 0;

}


int grabarMatriz()
{
  FILE *arch;
  char mat[MAXF][MAXC];
  char a;
  char lim = ',';
  char salto = '\n';
  int f = 0;
  int c = 0;

  arch = fopen("archivo.txt","wt");
  if (arch == NULL)
  {
      printf("No se pudo abrir archivo\n");
      return 1;

  }

  for(f=0; f<MAXF; f++)
    {
      for(c=0; c<MAXC; c++)
        {
            printf("Ingrese Valor fila %d columna %d: ", f+1, c+1);
            scanf("%c", &a );
            while(getchar()!='\n');
            fputc(a, arch);
            fputc(lim,arch);

        }
            fputc(salto, arch);

    }

    fclose(arch);
    return 0;
  }

int leerMatriz()
  {
    FILE *arch;

    char a;


    arch = fopen("archivo.txt","rt");
    if (arch == NULL)
    {
        printf("No se pudo abrir archivo\n");
        return 1;

    }

    a = fgetc(arch);
    while(!feof(arch))
    {
      while(a != '\n')
      {
          printf("%c",a);
          a=fgetc(arch);

      }
      printf("\n");
      a=fgetc(arch);
    }
      fclose(arch);
      return 0;
    }
