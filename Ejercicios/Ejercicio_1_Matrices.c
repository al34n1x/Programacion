
/*

Realizar una función que permita cargar con números enteros, una matriz de 5x5, ingresando los
datos desde teclado. ¿En qué cambiaría la función si la matriz contuviera otro tipo de datos y/o
tuviese otras dimensiones?

*/

#include <stdio.h>

#define MAXF 5
#define MAXC 5

int carga_datos(int mat[][MAXC]);

int main()
{
  int mat[MAXF][MAXC]={0};
  carga_datos(mat);

  return 0;


}


int carga_datos(int mat[MAXF][MAXC])
{
  int f = 0;
  int c = 0;

  for (f = 0; f<MAXF; f++)
    for (c=0; c<MAXC; c++)
      {
        printf("Ingrese valor para posición Fila %d Columna %d: ", f+1, c+1);
        scanf("%d", &mat[f][c]);
      }

  for (f = 0; f<MAXF; f++)
  {
      printf("\n");
      for (c=0; c<MAXC; c++)
          {
            printf("%4d", mat[f][c]);
          }
  }

printf("\n");
return 0;

}
