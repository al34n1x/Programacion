/*Realizar una función que reciba como parámetros día, mes y año indique si la fecha es un fecha

gregoriana válida.*/

#include <stdio.h>

int fecha (int a, int b, int c);
int bisiesto (int a);


int main ()
{

    int a = 0;
    int b = 0;
    int c = 0;

    printf("Ingrese día:");
    scanf("%d", &a);

    printf("Ingrese mes:");
    scanf("%d", &b);

    printf("Ingrese año:");
    scanf("%d", &c);

    (fecha(a,b,c)==1)?printf("Fecha Gregoriana\n"):printf("Fecha No Gregoriana\n");


}

int fecha(int x, int y, int z)

{

    /* x = dia, y = mes, z= año */
    int temp = 0;
    temp = z;
    if (bisiesto(temp)==1)
    {  if(y==1 || y==3 || y==5 || y==7 || y==8 || y==10 || y ==12)
        if(x>0 && x <32)
          return 1;
        else
          return 0;
      if(y==4 || y==6 || y==9 || y==11)
          if(x>0 && x <31)
            return 1;
          else
            return 2;
      if (y==2)
          if(x>0 && x<30)
            return 1;
          else
            return 2;
    }

    if (bisiesto(temp)!=1)
    {  if(y==1 || y==3 || y==5 || y==7 || y==8 || y==10 || y ==12)
          if(x>0 && x <32)
            return 1;
          else
            return 0;
      if(y==4 || y==6 || y==9 || y==11)
          if(x>0 && x <31)
            return 1;
          else
            return 2;
      if (y==2)
          if(x>0 && x <29)
            return 1;
          else
            return 2;
    }

}


int bisiesto(int x)
{

      if((x%4 == 0) && (x%100 == 0) && (x%400==0))
        return 1;
      if((x%4 == 0) && (x%100 != 0))
        return 1;
      if(x%4 != 0)
        return 2;

}
