/*Hacer una función que determine si una secuencia
de caracteres es capicúa.*/

#include <stdio.h>
#define MAX 80

int capicua(char cad[]);

int main()
{
  char cad[MAX];
  int valor = 0;
  printf("Ingrese Cadena: ");
  scanf("%s", cad );

  valor = capicua(cad);
  if(valor == 1)
    printf("Capicua\n");
  else
    printf("No Capicua\n");

  return 0;
}


int capicua(char cad[MAX])
{
  int i = 0;
  int j = 0;
  int flag = 1;

  for(i=0; cad[i]; i++)
    ;

  for(j=0; j<i; j++)
    {
        if(cad[j]!=cad[i-j-1])
          flag = 0;

    }

  return flag;

}
