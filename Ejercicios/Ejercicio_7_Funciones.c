/* Un banco necesita para sus cajeros automáticos un programa que lea una cantidad de dinero e

informe la cantidad mínima de billetes a entregar, considerando que existen billetes de $100, $50,

$10, $5 y $1. Realizar una función que calcule dichas cantidades.*/

#include <stdio.h>

int billetes (int a);


int main()
{
  int a = 0;

  printf("Ingrese monto:");
  scanf("%d", &a );

  printf("La minima cantidad de billetes a entregar es:%d\n", billetes(a));


}

int billetes(int a)
{
    int cant_100 = 0;
    int cant_50 = 0;
    int cant_10 = 0;
    int cant_5 = 0;
    int cant_1 = 0;

    if (a/100 > 1)
      {cant_100 = a/100; a = a - (cant_100*100);}
    if (a > 49)
      {cant_50= a/50; a = a - (cant_50*50);}
    if (a  > 9)
      {cant_10 = a/10; a = a - (cant_10*10);}
    if (a > 4)
      {cant_5= a/5; a = a - (cant_5*5);}
    if (a > 0)
      {cant_1 = a/1; a = a - (cant_1*1);}

    return (cant_1 + cant_5 + cant_10 + cant_50 + cant_100);



}
