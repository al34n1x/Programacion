/*Hacer una función que reciba como parámetro un puntero a la cabeza de una lista simple e
invierta el orden de sus nodos, sin destruir y crear nodos nuevos y sin intercambiar la información
contenida en los mismos.*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>

typedef struct snodo
{
  int valor;
  struct snodo *siguiente;

}tnodo;

typedef tnodo *tpuntero;

int insertarprincipio(tpuntero *_nodo, int valor);
int invertir(tpuntero *_nodo);
int imprimir(tpuntero *_cabeza);

int main()

{
    tpuntero cabeza;
    int e = 0;
    cabeza = NULL;
    printf("Ingrese elementos, -1 para terminar: ");
    scanf("%d", &e);

    while (e != -1)
      {
          insertarprincipio(&cabeza, e);
          printf("Ingrese elementos, -1 para terminar: ");
          scanf("%d", &e);

      }

    invertir(&cabeza);
    imprimir(&cabeza);
    return 0;
}

int insertarprincipio (tpuntero *_nodo, int valor)
{
    tpuntero aux;
    aux = (tpuntero)malloc(sizeof(tnodo));


    aux->valor = valor;
    aux->siguiente = *_nodo;
    *_nodo = aux;



  return 0;
}


int invertir(tpuntero *_cabeza)
{
    tpuntero aux, siguiente, previo;
    aux = (tpuntero)malloc(sizeof(tnodo));
    siguiente = (tpuntero)malloc(sizeof(tnodo));
    previo = (tpuntero)malloc(sizeof(tnodo));

    aux = *_cabeza;
    previo = NULL;
    if(aux!=NULL)
    {
        do {
            siguiente = aux->siguiente;
            aux->siguiente = previo;
            previo = aux;
            if(siguiente!=NULL)
                aux = siguiente;
            else
                *_cabeza = aux;
        } while(aux != *_cabeza);
    }

return 0;

}



int imprimir(tpuntero *_cabeza)
{
    tpuntero aux;
    aux = (tpuntero)malloc(sizeof(tnodo));
    aux = *_cabeza;
    while(aux!=NULL)
    {
        printf("%d\n", aux->valor);
        aux = aux->siguiente;

    }

  return 0;
}
