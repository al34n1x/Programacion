/* La tarifa de un videoclub para el alquiler de sus videos es: Dibujos = $2; Estrenos = $3; Otros =

$2.5. Además cobra por devolución tardía $1 por el primer día y $0.5 por cada no de los días

siguientes. Realizar una función que devuelva el alquiler a pagar por un cliente, sabiendo que

recibe como parámetros el tipo de video y la cantidad de días desfasados en devolución. Si la

devolución es anterior al día de devolución recibirá un entero negativo.*/

#include <stdio.h>

float alq_pagar(float a, float dias);


int main()

{
    int a = 0;
    float dias = 0;
    float precio = 0;
    float total = 0;

    printf("Ingrese tipo de video 1 Dibujos, 2 Estrenos, 3 Otros: ");
    scanf("%d", &a);

    printf("Ingrese días: ");
    scanf("%f", &dias);

    switch (a) {
      case 1: precio = 2; total = alq_pagar(precio, dias); break;
      case 2: precio = 3; total = alq_pagar(precio, dias); break;
      case 3: precio = 2.5; total = alq_pagar(precio, dias); break;
      default: break;
    }

    if (total != -1)
      printf("El precio a pagar es: %f\n", total);
    else
      printf("No hay sobrecargos\n");


}


float alq_pagar(float a, float dias)
{
    float i = 0;


    if (dias > 0)
      {
        i = a + (dias * 0.5);

        return i;
      }
    else
      return -1;


}
