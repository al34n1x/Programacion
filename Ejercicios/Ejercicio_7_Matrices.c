/*
Dada una matriz cargada utilizando la función del ejercicio anterior, realizar funciones para:

a) Almacenar en un vector la lluvia promedio caida en cada uno de los meses

b) Almacenar en un vector la lluvia promedio caida durante cada uno de los años

c) Mostrar los resultados obtenidos en los puntos a y b

*/

#include <stdio.h>
#include <time.h>
#define MAXF 10
#define MAXC 12


int prom_mes(float *ptr, int matriz[][MAXC]);
int prom_anual(float *ptr, int matriz[][MAXC]);

int main()
{
    int mat[MAXF][MAXC];
    float mes[MAXC] = {0};
    float anual[MAXF] = {0};

    int f = 0;
    int c = 0;


    srand(time(NULL));

    for (f=0; f<MAXF; f++)
      for(c=0; c<MAXC; c++)
        mat[f][c] = rand()%(999+1);

    for(f=0; f<MAXF; f++)
      {
        printf("\n");
        for(c=0; c<MAXC; c++)
          printf("%4d", mat[f][c]);
      }

    printf("\n");

    prom_mes(mes, mat);

    for (c=0; c<MAXC; c++)
      printf("%4f\n", mes[c]);
    printf("\n");

    prom_anual(anual, mat);

    printf("\n");

    for (f=0; f<MAXF; f++)
      printf("%4f\n", anual[f]);
    printf("\n");

    return 0;

}

int prom_mes(float *mes, int mat[MAXF][MAXC])
{
  int f = 0;
  int c = 0;
  float vector[MAXC] = {0};
  float suma = 0;

  for(c=0; c<MAXC; c++)
  {
    for(f=0; f<MAXF; f++)
      suma = mat[f][c];

    vector[c] = suma / (int)MAXF;
    suma = 0;
  }

  for (c=0; c<MAXC; c++)
    *(mes+c) = vector[c];

  return 0;

}

int prom_anual(float *anual, int mat[MAXF][MAXC])
{

  int f = 0;
  int c = 0;
  float vector[MAXF] = {0};
  float suma = 0;

  for(f=0; f<MAXF; f++)
  {
    for(c=0; c<MAXC; c++)
      suma = mat[f][c];

    vector[f] = suma / (int)MAXC;
    suma = 0;
  }

  for (f=0; f<MAXF; f++)
    *(anual+f) = vector[f];

  return 0;


}
