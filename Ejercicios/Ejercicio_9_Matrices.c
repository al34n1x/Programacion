/*
Dada una matriz de dimension NxN. realizar las siguientes operaciones:

a) Intercambiar dos filas cualquiera.

b) Intercambiar dos columnas cualesquiera.

c) Intercambiar una fila y una columna dada

d) Transponer la matriz sobre si misma
*/

#include <stdio.h>
#include <time.h>

#define MAXF 3
#define MAXC 3

int filas(int a, int b, int mat[][MAXC]);
int columnas(int a, int b, int mat[][MAXC]);
int transponer(int mat[][MAXC]);

int main()
{
  int mat[MAXF][MAXC] = {0};
  int f = 0;
  int c = 0;
  int a = 0;
  int b = 0;

  srand(time(NULL));


  for(f=0; f<MAXF; f++)
    for (c=0; c<MAXC; c++)
      mat[f][c] = rand()%(10+1);

  do
  {
    printf("Ingrese valor de A: ");
    scanf("%d",&a);
  } while (a>MAXF);

  do
    {
      printf("Ingrese valor de B: ");
      scanf("%d",&b);
   }while(b>MAXC);

   for (f=0; f<MAXF; f++)
     {
       printf("\n");
       for(c=0; c<MAXC; c++)
         printf("%4d", mat[f][c]);
     }


   filas(a,b,mat);
   columnas(a,b,mat);
   transponer(mat);

   return 0;

}

int filas(int a, int b, int mat[MAXF][MAXC])
{

  int f = 0;
  int c = 0;
  int auxc[MAXC] = {0};

  for (c=0; c<MAXC; c++)

      {
          auxc[c] = mat[a][c];
          mat[a][c] = mat[b][c];
          mat[b][c] = auxc[c];
      }
  printf("\n\n");
  for (f=0; f<MAXF; f++)
    {
      printf("\n");
      for(c=0; c<MAXC; c++)
        printf("%4d", mat[f][c]);
    }

  printf("\n");
  return 0;


}


int columnas(int a, int b, int mat[MAXF][MAXC])
{

  int f = 0;
  int c = 0;
  int auxf[MAXF] = {0};

  for (f=0; f<MAXF; f++)

      {
          auxf[f] = mat[f][a];
          mat[f][a] = mat[f][b];
          mat[f][b] = auxf[f];
      }
  printf("\n\n");
  for (f=0; f<MAXF; f++)
    {
      printf("\n");
      for(c=0; c<MAXC; c++)
        printf("%4d", mat[f][c]);
    }

  printf("\n");
  return 0;


}



int transponer(int mat[MAXF][MAXC])
{

  int f = 0;
  int c = 0;
  int a = 0, b = 0;
  int aux[MAXF][MAXC] = {0};

  for (f=0; f<MAXF; f++)
  {
    for(c=0; c<MAXC; c++)

      {
          aux[f][c] = mat[a][b];
          b++;
      }

      b=0;
      a++;
  }

  printf("\n\n");
  for (f=0; f<MAXF; f++)
    {
      printf("\n");
      for(c=0; c<MAXC; c++)
        printf("%4d", aux[f][c]);
    }

  printf("\n");
  return 0;


}
