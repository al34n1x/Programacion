/*
Realice una función devuelva verdadero si todas sus columnas de una matriz son palíndromos.
*/

#include <stdio.h>

#define MAXF 3
#define MAXC 3

int palindromos(int mat [][MAXC]);


int main()

{

  int f = 0;
  int c = 0;
  int mat[MAXF][MAXC] = {0};
  int status = 0;

  for (f=0; f<MAXF; f++)
    for(c=0; c<MAXC; c++)
    {
        printf("Ingrese valor para posición fila %d, columna %d: ", f+1, c+1);
        scanf("%d",&mat[f][c]);
    }

    for (f = 0; f<MAXF; f++)
    {
        printf("\n");
        for (c=0; c<MAXC; c++)
            {
              printf("%4d", mat[f][c]);
            }
    }
  printf("\n");

  status = palindromos(mat);

  if(status == 1)
    printf("palindromos\n");
  else
    printf("No Palindromos\n");

  return 0;
}


int palindromos (int mat [MAXF][MAXC])
{
  int f = 0;
  int sup = MAXF - 1;
  int c = 0;
  int flag = 1;

    while ((c < MAXC) && (flag == 1))
      {
        while ((f < MAXF) && (flag ==1))
        {
            if (mat[f][c]==mat[sup][c])
            {
                f++;
                sup--;
            }
            else
              flag = 0;

        }
        c++;

     }

    return flag;

}
