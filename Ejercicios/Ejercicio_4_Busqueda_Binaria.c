/*
Realizar una función que devuelva la posición en que se haya un valor dado mediante el método

de búsqueda binaria. En caso que no lo encuentre devolver -1,.
*/

#include <stdio.h>
#define max 10

int busqueda(int v, int m[]);

int main()
{

  int vec[max]={0};
  int i = 0;
  int valor = 0;
  int pos = 0;

  for(i=0; i<max; i++)
    vec[i]=i+1;

    for(i=0; i<max; i++)
      printf("%d\n",vec[i] );

  printf("Ingrese Valor a buscar:");
  scanf("%d", &valor);

  pos = busqueda(valor, vec);
  if (pos >= 0)
    printf("El valor %d se encuentra en la posición %d\n",valor, pos );

  else
    printf("El valor %d no se encuentra dentro del vector\n", valor );

  return 0;

}

int busqueda(int v, int vec[])
{

  int inf = 0;
  int sup = max;
  int mit = 0;
  int valor = 0;


  mit = (sup+inf)/2;


  while (inf<=sup)
    {


        if(vec[mit] == v)
          return mit;

        if(vec[mit] > v)
          {
            sup = mit;
            mit = ((sup+inf)/2);
          }

        else
        {
          if(vec[mit] < v)
          {
            inf = mit;
            mit = ((sup+inf)/2);

          }
        }

    }

}
