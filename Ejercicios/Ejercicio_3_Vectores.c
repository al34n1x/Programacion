/* Realizar una función que devuelva el mínimo elemento de un vector.*/

#include <stdio.h>
#define max 5

int minimo(int *ptr);

int main()
{

  int vec[max] = {0};
  int min_valor = 0;
  int i = 0;

  for (i = 0; i<max; i++)
	{
		printf("Ingrese valor para la posición %d:", i + 1);
		scanf("%d", &vec[i]);
	}

  printf("El minimo valor del vector es %d\n", minimo(&vec[0]));

  return 0;

}


int minimo(int *ptr)
{
    int i = 0;
    int min = *ptr;

    for (i=0; i<max; i++)
    {
        if (*ptr < min)
        {
          min = *ptr;
        }
          ptr = ptr + 1;
    }

    return min;

}
