#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

typedef struct snodo{

  int valor;
  struct tnodo *sig;

}tnodo;

typedef tnodo *tpuntero;

int apilar(tpuntero *cabeza, int valor);
int imprimir(tpuntero *cabeza);

int main()
{
    tpuntero cabeza;
    int a = 0;
    cabeza = NULL;

    printf("Ingrese Valor, -1 para salir: ");
    scanf("%d", &a);

    while(a!=-1)
    {
    apilar(&cabeza, a);
    printf("Ingrese Valor, -1 para salir: ");
    scanf("%d", &a);
    }

    imprimir(&cabeza);
    return 0;

}

int apilar(tpuntero *cabeza, int valor)
{

  tpuntero nuevo;
  nuevo = (tpuntero)malloc(sizeof(tnodo));
  nuevo->valor = valor;
  nuevo->sig = *cabeza;
  *cabeza = nuevo;


  return 0;

}


int imprimir(tpuntero *cabeza)
{
  tpuntero nuevo;
  nuevo = (tpuntero)malloc(sizeof(tnodo));
  nuevo = *cabeza;

  while(nuevo!=NULL)
  {
    printf("%d\n", nuevo->valor);
    nuevo = nuevo ->sig;

  }

  return 0;

}
