/*
Realizar una función que determine si una matriz cuadrada
de dimensión N es simétrica con
respecto a su diagonal principal.
*/

#include <stdio.h>

#define MAXF 3
#define MAXC 3

int simetrica(int mat[][MAXC]);

int main()
{

  int mat[MAXF][MAXC] = {0};
  int f = 0;
  int c = 0;
  int valor = 0;

  for (f = 0; f < MAXF; f++)
    for(c = 0; c < MAXC; c++)
    {
      printf("Ingrese valor para posición Fila %d Columna %d: ", f+1, c+1);
      scanf("%d", &mat[f][c]);
    }

    valor = simetrica(mat);

    if(valor == 1)
      printf("Matriz Simetrica\n");
    else
      printf ("Matriz no Simetrica\n");

    return 0;


}

int simetrica(int mat[MAXF][MAXC])
{

  int f = 0;
  int c = 0;
  int sim = 1;

    for(f = 0; f < MAXF; f++)
      for (c = 0; c < MAXC; c++)


    while ((f<MAXF) && (sim == 1))
      {

          while((c<MAXC) && (sim == 1))

            {
                if(mat[f][c] == mat[c][f])
                  {
                      c++;
                  }
                else
                  sim = 0;

            }

          f ++;
          c = 0;
      }

  return sim;
}
