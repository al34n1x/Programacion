/*Definir estructuras para almacenar los siguientes tipos de datos:
a) fecha
b) hora
c) Datos de un producto
código de barras (21 dígitos)
nombre (cadena de caracteres)
precio unitario (número real)
stock (número entero)
fecha de actualización de stock (día, mes, año)
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

struct sfecha
{
  int day;
  int month;
  int year;

};


struct sproducto
{
  char cod[22];
  char nombre[100];
  float precio;
  int stock;
  struct sfecha fechastock;

};

int update(struct sfecha *a, struct sproducto *b);


int main()
{

  struct sproducto productos;
  struct sfecha fecha;

  update(&fecha, &productos);

  puts(productos.cod);
  puts(productos.nombre);
  printf("%f\n",productos.precio );
  printf("%d\n",productos.stock );
  printf("%d\n",productos.fechastock.day);
  printf("%d\n",productos.fechastock.month);
  printf("%d\n",productos.fechastock.year);

  return 0;
}

int update(struct sfecha *fecha, struct sproducto *producto)
{
  printf("Ingrese Codigo de Barras: ");
  gets(producto->cod);

  printf("Ingrese Nombre del Producto: ");
  gets(producto->nombre);
  printf("Ingrese Precio: ");
  scanf("%f",&producto->precio );
  printf("Ingrese Stock: ");
  scanf("%d", &producto->stock );
  printf("Ingrese Dia: ");
  scanf("%d", &producto->fechastock.day);
  printf("Ingrese mes: ");
  scanf("%d", &producto->fechastock.month);
  printf("Ingrese año: ");
  scanf("%d", &producto->fechastock.year);

  return 0;

}
