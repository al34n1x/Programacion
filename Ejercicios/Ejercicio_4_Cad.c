/*Hacer una función que permita reemplazar en una secuencia de caracteres todos los guiones (-)
por el carácter numeral (#).*/


#include <stdio.h>

#define MAX 80

int reemplazo(char cad[]);

int main()
{
  char cad[MAX];

  printf("Ingrese Cadena: ");
  scanf("%s", cad);

  reemplazo(cad);

  return 0;

}


int reemplazo(char cad[MAX])
{

  int i = 0;

  while(cad[i])
    {
        if(cad[i]=='-')
          cad[i] = '#';
        i++;

    }

    printf("%s\n", cad);

    return 0;

}
