
/*
Hacer una función que cargue una matriz con números al azar entre 0 y 999, la cual representa la
lluvia mensual caída del año 1995 al 2008. Las filas representan los años y las columnas
correponden a los 12 meses del año.
*/


#include <stdio.h>
#include <time.h>
#define MAXF 10
#define MAXC 12

int lluvias(int mat [][MAXC]);

int main()
{
    int mat[MAXF][MAXC];

    srand(time(NULL));

    lluvias(mat);

    return 0;

}

int lluvias(int mat[MAXF][MAXC])
{
  int f = 0;
  int c = 0;

  for (f=0; f<MAXF; f++)
    for(c=0; c<MAXC; c++)
      mat[f][c] = rand()%(999+1)+1;

  for(f=0; f<MAXF; f++)
    {
      printf("\n");
      for(c=0; c<MAXC; c++)
        printf("%4d", mat[f][c]);
    }

  printf("\n");

  return 0;
}
