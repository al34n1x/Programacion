
/* Ingresar un número N entero positivo y calcular e imprimir la suma de los N primeros números naturales.*/

#include <stdio.h>

int main ()
{

  int a, i=1;
  int res = 0;

  printf("Ingresar numero entero:");
  scanf("%d", &a );


  for (i=1; i<=a; i++)
  {
    res = res + i;

  }

  printf("El resultado es:%d\n", res);


  return 0;

}
