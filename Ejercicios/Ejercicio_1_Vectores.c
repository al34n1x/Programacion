/*Realizar una función que imprima los valores de un vector de N posiciones, desde la posición

máxima a la posición mínima.*/

#include <stdio.h>
#define max 5

int imprimir(int *ptr);


int main()
{
	int vec[max] = { 0 };
	int i = 0;

	for (i = 0; i<max; i++)
	{
		printf("Ingrese valor para la posición %d:", i + 1);
		scanf("%d", &vec[i]);
	}

	imprimir(&vec[0]);

	system("pause");

	return 0;
}

int imprimir(int *ptr)
{
	int i = 0;
	for (i = 0; i<max; i++)
		printf("Posición %d - Valor:%d", i + 1, *ptr + i);

	return 0;

}
