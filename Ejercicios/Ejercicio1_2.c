#include <stdio.h>


/* Ingresar desde el teclado un carácter que representa la operación a
realizar y dos números cualquiera, luego calcular e imprimir el resultado de
aplicar dicha operación sobre los números ingresados. Los caracteres que representan
las operaciones son S suma, R resta, M multiplicacion, D división, T resto.  */


int main()

{

  char oper;
  float a=0,b=0,res=0;

  printf("Ingrese Valor a:");
  scanf("%f", &a );
  printf("Ingrese Valor b:");
  scanf("%f", &b );


  printf("Ingrese Operacion: S suma, R resta, M multiplicacion, D división, T resto:");
  scanf("%s", &oper );

  switch (oper) {
    case 's': res = a + b; break;
    case 'r': res = a - b; break;
    case 'm': res = a * b; break;
    case 'd': res = a / b; break;
    case 't': res = (int) a % (int) b; break;
    default: printf("Valor Incorrecto\n"); break;
  }

  printf("El resultado de la operacion es: %f\n", res );


  return 0;


}
