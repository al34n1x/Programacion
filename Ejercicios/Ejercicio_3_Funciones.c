
/* Realizar una función que reciba el año como parámetro y devuelva verdadero si el año es bisiesto

o falso si no lo es. Se recuerda que un año es bisiesto cuando es divisible por 4. Sin embargo

aquellos años que sean divisibles por 4 y también por 100 no son bisiestos, a menos que también

sean divisibles por 400. Por ejemplo, 1900 no fue bisiesto pero sí lo fueron el 2000 y 2004. */

#include <stdio.h>

int bisiesto (int a);


int main ()

{
    int a = 0;

    printf("Ingrese año:");
    scanf("%d", &a);

    (bisiesto(a)==1)?printf("Año Bisiesto\n"):printf("No año Bisiesto\n");

    return 0;


}


int bisiesto(int x)
{

      if((x%4 == 0) && (x%100 == 0) && (x%400==0))
        return 1;
      if((x%4 == 0) && (x%100 != 0))
        return 1;
      if(x%4 != 0)
        return 2;

}
