/*Escribir una función strright(&lt;cadena&gt;,&lt;n&gt;) que permita obtener una subcadena de caracteres
con los últimos &lt;n&gt; caracteres de la cadena pasada como parámetro. La función no hará nada si
el parámetro &lt;n&gt; es menor que cero o mayor que la longitud de la cadena.*/

#include <stdio.h>
#include <string.h>
#define MAX 80

int strright(char cad[MAX], int n);

int main()
{
  char cad[MAX];
  int n = 0;

  printf("Ingrese Cadena: ");
  gets(cad);
  printf("Ingrese desde que pos quiere obtener la subcadena: ");
  scanf("%d",&n);

  //n= n-1;

  strright(cad, n);
  return 0;

}

int strright (char cad[MAX], int n)
{
  int i=0;
  int j = 0;
  int pos = 0;
  char aux[MAX];

  i=strlen(cad);
  pos = i - n;
  if (n>0 && n<i)
    while(pos < i)
      {
        aux[j] = cad[pos];
        pos ++;
        j++;
    }
  aux[j] = '\0';

  puts(aux);

  return 0;
}
