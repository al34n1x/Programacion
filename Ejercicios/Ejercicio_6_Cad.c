/*
Hacer una función strNcpy() que permita extraer una sub-cadena, indicando la posición inicial y la
cantidad de caracteres. Ejemplo, dada la secuencia: “El número de teléfono es 4356-7890. “
extraer la sub-cadena que comienza en la posición 26, la cantidad de 9 caracteres.
*/

#include <stdio.h>
#define MAX 80

int strNcpy(int init, int cant, char cad[]);


int main()
{
  char cad[MAX];
  int init = 0, cant = 0;
  printf("Ingrese Cadena: ");
  gets(cad);

  printf("Ingrese Pos Inicial: ");
  scanf("%d", &init);

  printf("Ingrese cantidad de caracteres: ");
  scanf("%d", &cant);
  cant = cant + 1; 
  strNcpy(init, cant, cad);
  return 0;

}


int strNcpy(int init, int cant, char cad[MAX])

{
    int i = 0;
    int j = 0;
    int k = 0;
    char aux[MAX];
    for (i=0; cad[i]; i++)
      ;
    j=(init-1);
      while((cant!=0) && j<i)
        {
          aux[k] = cad[j];
          j++;
          k++;
          cant --;
        }
    aux[k]='\0';

    puts(aux);

    return 0;
}
