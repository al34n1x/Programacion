/*
Un supermercado desea contar con un programa que le permita registrar los datos de sus
artículos. En el mismo no existen más de 1000 productos distintos. Realizar un programa ABM
utilizando la estructura indicada en el punto 1.c, en el que los datos deberán estar ordenados por
nombre. Contemplar las siguientes funciones:

 altaProd: parámetros recibidos serán el vector y la cantidad cargados hasta el momento. Esta
función deberá permitir cargar productos hasta que se llene el vector o hasta que ingresen un
nombre vacío en el campo nombre.

 bajaProd: parámetros recibidos serán el vector y la cantidad cargados hasta el momento. Esta
función deberá permitir ingresar un nombre y eliminar los datos del producto.

 mostrarProductos: parámetros recibidos serán el vector y la cantidad cargados hasta el
momento. Esta función deberá permitir mostrar los datos de todos los productos del
supermercado.
*/

#include <stdio.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include <stdlib.h>


#define MAX 2

struct sfecha
{
	int day;
	int month;
	int year;

};


struct sproducto
{
	char cod[22];
	char nombre[100];
	float precio;
	int stock;
	struct sfecha fechastock;

};

void cargar(struct sproducto b[MAX]);
void mostrar(struct sproducto b[MAX]);
void baja(char cad[100],struct sproducto b[MAX]);


int main()
{

	struct sproducto producto[MAX];
	struct sfecha fecha;
  char nombre [100];
	cargar(producto);
	mostrar(producto);

  printf("Ingrese Producto a Eliminar: ");
  gets(nombre);
  baja(nombre, producto);
  mostrar(producto);

	system("pause");

	return 0;

}

void cargar(struct sproducto producto[MAX])
{
	int i = 0;




	while ((strcmp(producto[i].nombre, " ") != 0) && (i<MAX))
	{


    printf("\nIngrese Nombre: ");
    gets(producto[i].nombre);

    printf("Ingrese Precio: ");
		scanf("%f", &producto[i].precio);

		printf("Ingrese Stock: ");
		scanf("%d", &producto[i].stock);
		while (getchar() != '\n');
		printf("Ingrese Cod: ");
		gets(producto[i].cod);

		printf("Ingrese dia: ");
		scanf("%d", &producto[i].fechastock.day);

		printf("Ingrese mes: ");
		scanf("%d", &producto[i].fechastock.month);

		printf("Ingrese año: ");
		scanf("%d", &producto[i].fechastock.year);


		i++;
    while (getchar() != '\n');

	}


}


void mostrar(struct sproducto producto[MAX])
{
	int i = 0;
	printf("\n");
	do
	{
		puts(producto[i].nombre);
		puts(producto[i].cod);
		printf("%4f\n", producto[i].precio);
		printf("%4d\n", producto[i].stock);
		printf("%4d\n", producto[i].fechastock.day);
		printf("%4d\n", producto[i].fechastock.month);
		printf("%4d\n", producto[i].fechastock.year);

		i++;
	} while ((strcmp(producto[i].nombre, " ") != 0) && (i<MAX));

	printf("\n");

}

void baja(char cad[100], struct sproducto producto[MAX])
{

  int i = 0;

  while(i<MAX)
  {
      if (strcmp(producto[i].nombre,cad)==0)
      {
          strcpy(producto[i].nombre," ");
          strcpy(producto[i].cod, "00000");
          producto[i].precio = 0;
          producto[i].stock = 0;
          producto[i].fechastock.month = 0;
          producto[i].fechastock.day = 0;
          producto[i].fechastock.year = 0;

      }
      i++;

  }

}
