
/* Hacer una función que permita eliminar todos los comentarios de una o varias líneas de un
programa fuente escrito en lenguaje “C” */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>


int eliminar_comentarios(FILE *arch);

int main()

{

  FILE *arch;

  arch = fopen("Ejercicio_1_Funciones.c","rt");
  if (arch == NULL)
  {
    printf("No se pudo abrir el archivo");
    return 1;

  }

  eliminar_comentarios(arch);

  fclose(arch);

  return 0;

}


int eliminar_comentarios (FILE *arch)
{

  FILE *aux;
  char c;

  aux = fopen("Ejercicio_1_Funciones_sinComentarios.c","wt");
  if (aux == NULL)
  {
    printf("No se pudo abrir el archivo");
    return 1;

  }

  c = fgetc(arch);

  while(!feof(arch))
  {

      if (c == '/')
      {
        c = fgetc(arch);
          while(c != '/')
            {
              c = fgetc(arch);
            }
      }

      if (c=='/')
        {
          c = fgetc(arch);
        }
      fputc(c, aux);

      c = fgetc(arch);

  }

  fclose(aux);

  return 0;

}
