/* Realizar una función que determine si todos los elementos
de dos vectores son iguales y están en el mismo orden.*/

#include <stdio.h>
#define max 10

int iguales(int *ptr, int *ptr_1);


int main()

{
    int vec1[max]={0};
    int vec2[max]={0};
    int i = 0;

    for (i=0; i<max; i++)
      {
        printf("Ingrese valor %d para Pila 1:", i+1);
        scanf("%d",&vec1[i]);
      }

    for (i=0; i<max; i++)
        {
          printf("Ingrese valor %d para Pila 2:", i+1);
          scanf("%d",&vec2[i]);
        }

    (iguales(&vec1[0],&vec2[0])==1)?printf("Vectores Iguales\n"):printf("Vectores Diferentes\n");

    return 0;

}


int iguales(int *a, int *b)
{

  int i = 0;
  int flag = 1;
  while((i<max) && (flag !=-1))
    {
      if(*(a+i) = *(b+i))
        i++;
      else
        flag = -1;
    }
  return flag;

}
