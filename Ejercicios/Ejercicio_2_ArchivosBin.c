/*Un laboratorio de especialidades medicinales posee un archivo en disco cuyos registros contienen
los siguientes campos:
Lote: 5 caracteres
Código de barras: 21 caracteres
Nombre: 40 caracteres
Costo: número real
Stock: Número entero: Cantidad de envases almacenados
Mes, Año: Enteros: Mes y año de vencimiento de la medicación

El archivo contiene un registro por cada lote, es decir que pueden existir varios registros para un
mismo medicamento si éstos pertenecen a lotes distintos. Se solicita desarrollar uno o más
programas para:

 Crear el archivo, ingresando los datos por teclado.
 Borrar del archivo los registros de aquellos lotes que se encuentren vencidos,
comparando la fecha de vencimiento de cada uno con una fecha testigo que se ingresa
por teclado. Para borrar los registros es necesario generar un nuevo archivo que
contenga solamente los lotes no vencidos, emitiendo además un listado por pantalla con
los registros eliminados.
 Generar e imprimir un listado con el código, el nombre y la cantidad de unidades en stock
para cada medicamento, sin importar el lote al que pertenezca. El archivo no está
ordenado, y los medicamentos son a lo sumo 130.*/


#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <time.h>
#define MAX 5

struct sfecha{
  int dia;
  int mes;
  int ano;
};

struct sprod {

  char lote[6];
  char cod[22];
  char nombre[41];
  float costo;
  int stock;
  struct sfecha fechastock;
};


int carga(struct sprod b[MAX]);
int imprimir();


int main()
{
  struct sprod producto[MAX];

  carga(producto);
  imprimir();


  return 0;

}

int carga (struct sprod producto[MAX])
{
  FILE *arch;
  int i=0;

  arch = fopen ("productos.dat","ab+");
    if(arch == NULL)
      {
          printf("No se pudo abrir/encontrar el archivo");
          return 1;

      }

    printf("Ingrese Lote: ");
    gets(producto[i].lote);

    while(strcmp(producto[i].lote,"")!=0)
    {
      printf("Ingrese Cod: ");
      gets(producto[i].cod);
      printf("Ingrese Nombre: ");
      gets(producto[i].nombre);
      printf("Ingrese Precio: ");
      scanf("%f",&producto[i].costo);
      printf("Ingrese stock: ");
      scanf("%d",&producto[i].stock );
      printf("Ingrese Dia: ");
      scanf("%d",&producto[i].fechastock.dia);
      printf("Ingrese mes: ");
      scanf("%d",&producto[i].fechastock.mes);
      printf("Ingrese año: ");
      scanf("%d",&producto[i].fechastock.ano);

      while(getchar()!='\n');
      i++;
      printf("Ingrese Lote: ");
      gets(producto[i].lote);

    }

    fwrite(producto,sizeof(struct sprod), i, arch);

    fclose(arch);

    return 0;
}

int imprimir()
{
  FILE *arch;
  struct sprod producto;


  arch = fopen("productos.dat","rb+");
  if(arch==NULL)
  {
    printf("No se puede abrir el archivo\n");
    return 1;
  }

  fread(&producto, sizeof(struct sprod), 1, arch);

  while(!feof(arch))
  {
      puts(producto.lote);
      puts(producto.cod);
      puts(producto.nombre);
      printf("%f",producto.costo);

      fread(&producto, sizeof(struct sprod), 1, arch);

  }

  fclose(arch);
  return 0;

}
