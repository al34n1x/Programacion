/*
Desarrollar las siguientes funciones para manejar “tiempos”:

a) Ingresar un horario (horas, minutos, segundos) desde teclado, validando que sea correcto

b) Calcular la diferencia de horas. En el caso que la primera fecha es mayor a la segunda,
considerar que la primera hora corresponde a la hora del día anterior. La diferencia en horas
no supera las 24 horas.

c) Generar una cadena con formato hh:mm:ss
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#include <string.h>

struct shorario
{
  int hora;
  int min;
  int seg;

};

void carga (struct shorario *a, struct shorario *b);
void horario(struct shorario *a, struct shorario *b);

int main()
{
  struct shorario horario_a;
  struct shorario horario_b;

  carga(&horario_a, &horario_b);
  horario(&horario_a,&horario_b);

  return 0;
}


void carga(struct shorario *a, struct shorario *b)
{

  printf("Ingrese hora A: ");
  scanf("%d",&a->hora);
  printf("Ingrese min A: ");
  scanf("%d",&a->min);
  printf("Ingrese seg A: ");
  scanf("%d",&a->seg);

  printf("Ingrese hora B: ");
  scanf("%d",&b->hora);
  printf("Ingrese min B: ");
  scanf("%d",&b->min);
  printf("Ingrese seg A: ");
  scanf("%d",&b->seg);

}

void horario(struct shorario *a, struct shorario *b)
{

  int total_a = 0;
  int total_b = 0;
  float dif = 0;
  float calc_hora = 0;
  float calc_min = 0;
  float calc_seg = 0;
  int hora = 0;
  int min = 0;
  int seg = 0;



  total_a = ((a->hora*60*60)+(a->min*60)+(a->seg));

  total_b = ((b->hora*60*60)+(b->min*60)+(b->seg));

  if (total_b>total_a)
  {
    dif = total_b - total_a;
    calc_hora = (dif/60)/60;
    hora = (int)calc_hora;
    calc_min = (calc_hora - hora)*60;
    min = (int)calc_min;
    calc_seg = (calc_min - min)*60;
    seg = (int)calc_seg;
  }

  else
  {
    dif = (86400) - (total_a - total_b);
    printf("%f\n", dif);
    calc_hora = (dif/60)/60;
    hora = (int)calc_hora;
    calc_min = (calc_hora - hora)*60;
    min = (int)calc_min;
    calc_seg = (calc_min - min)*60;
    seg = (int)calc_seg;
  }

  printf("%d:%d:%d\n", hora, min, seg);

}
